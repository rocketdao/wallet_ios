//
//  RWWallet.swift
//  Rocket Wallet Swift
//
//  Created by Иван Петрашко on 20.07.2018.
//  Copyright © 2018 Иван§ Петрашко. All rights reserved.
//

import UIKit
import Realm
import RealmSwift
import Web3
import KeychainSwift
import EthereumKit

class RWWallet: Object {
    @objc dynamic var publicKey = ""
   // @objc dynamic var privateKey = ""
    @objc dynamic var name = ""
    @objc dynamic var ethBalanceLastUpdate: Date?
    var tokens = List<RWToken>()
    var allTokens = List<RWToken>()
    var transactions:List<RWTransaction> = List.init()
    @objc dynamic var setting:RWSetting! = nil
    var balanceList:List<RWBalance> = List.init()
    @objc dynamic var change24hPercent:NSNumber = 0.0
    @objc dynamic var change24hSum:NSNumber = 0.0
    
    class func getWallet()->RWWallet{
        let realm = try! Realm()
        if(realm.objects(RWWallet.self).first != nil){
            return realm.objects(RWWallet.self).first!
        } else {
            return RWWallet.init()
        }
    }
    
    func getChange24(block: @escaping (_ change: Bool) -> Void, blockError: @escaping (_ string: String?)->Void){
        RWBalance.getBalanceList(tokens: self.tokens, dateFrom: Date().minusDay()!, dateTo: Date(), points: NSNumber(integerLiteral: 2), block: { (list) in
            if list.count > 0 {
             let sumchange = (list.last?.totalUSD)! - (list.first?.totalUSD)!
            let realm = try! Realm()
            try! realm.write {
                self.change24hSum = NSNumber(floatLiteral:sumchange)
                self.change24hPercent = NSNumber(floatLiteral: sumchange*100/(list.last?.totalUSD)!)
                //block(true)
            }
            
            for token in self.tokens {
                let tokenLast = list.last?.tokenTypes.filter({$0.token.uppercased() == token.symbol.uppercased()}).first
                print(token)
                print(tokenLast)
                let sumlast = (tokenLast?.balance)! * (tokenLast?.rate)!
                
                let tokenFirst = list.first?.tokenTypes.filter({$0.token.uppercased() == token.symbol.uppercased()}).first
                let sumfirst = (tokenFirst?.balance)! * (tokenFirst?.rate)!
                
                
                let sumchange2 = sumlast - sumfirst
                
                print(tokenFirst?.token)
                print(sumchange2)
               
                //let num =
                var sumPercent = NSNumber(floatLiteral: sumchange2*100/(sumlast))
                
                if sumPercent == NSDecimalNumber.notANumber{
                    
                   sumPercent = NSNumber(integerLiteral: 0)
                } else {
                    
                }
                print("sumPercent")
                print(sumPercent)
                
             //    print(sumPercent)
                
                try! realm.write {
                    token.change24h = NSNumber(floatLiteral: sumPercent.doubleValue)
                    //
                }
                // let sumchange = (list.last?.tokenTypes)! - (list.first?.totalUSD)!
               // token.change24h =
            }
           
            block(true)
            //self.balanceList = list
                //block(list)
            } else {
                       block(true)
            }
        }) { (str) in
                //blockError(str)
        }
    }
    
    func getPrivateKey()->String!{
        let keychain = KeychainSwift()
        return keychain.get("privateKey")
    }
    
    func getPhase()->String!{
      //  EthereumPrivateKey
      /*
        let mnemonic = self.textview.text
            .split(separator: " ")
            .map(String.init)
        let seed = try Mnemonic.createSeed(mnemonic: mnemonic)
        let wallet = HDWallet(seed: seed, network: .main)
        
    
        
        do {
            let privateKey = try wallet.generatePrivateKey(at: 0)
            self.getPrivateByString(str: privateKey.raw.toHexString())
        } catch let error {
            print("Error: \(error)")
            let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }*/
       return "ok"
    }
    
    func getETHToken()->RWToken{
        return self.getSortTokens().filter({$0.symbol == "ETH"}).first!
    }
    
    class func export(withPrivateKey key: String?, andBlock block: @escaping (_ wallet: RWWallet?) -> Void, andError blockError: @escaping (_ string: String?) -> Void) {
        let wallet = RWWallet()
        
        //wallet.publicKey = "0x9F00678429cb91Bc264462C36C23e2031176f645"
        
        let keychain = KeychainSwift()
        keychain.set(key!, forKey: "privateKey")
        
        //Это откомментить
        if key != nil && key != ""{
        let privateKey = try? EthereumPrivateKey(
            hexPrivateKey: key!
        )
        wallet.publicKey = (privateKey?.address.hex(eip55: false))!
       // wallet.privateKey = key!
        } else {
            wallet.publicKey = "0x9F00678429cb91Bc264462C36C23e2031176f645"
        }
        let set = RWSetting()
        set.mix = "123123"
        
        wallet.setting = set
        
        print(wallet)
        
        RWToken.getMyToken(withPublicKey: wallet.publicKey, andBlock: { tokens in
             let realm = try! Realm()
            wallet.tokens = List.init()
              try! realm.write {
            for tok: RWToken in tokens! {
              
                    wallet.tokens.append(tok)
                }
                
            }
            
           
            try! realm.write {
                wallet.allTokens = API.sharedInstance.getTokenList()
            }
            try! realm.write {
                realm.add(wallet)
            }
            block(wallet)
        }, andError: { string in
        })
    }
    
    func getTransaction(block: @escaping (_ complete:Bool) -> Void,blockError: @escaping (_ string: String?) -> Void){
        let listOne = self.tokens
        RWTransaction.getTransaction(tokens: listOne, fromDate: Date(timeIntervalSinceReferenceDate: -123456789.0), todate: Date(), block: { (transaction) in
            self.transactions = transaction
            block(true)
        }) { (error) in
            // print(error)
            blockError(error)
        }
    }
    
    func getBalanceList(dateFrom:Date,dateTo:Date,block: @escaping (_ balance: List<RWBalance>) -> Void, andError blockError: @escaping (_ string: String?) -> Void){
        RWBalance.getBalanceList(tokens: self.tokens, dateFrom: dateFrom, dateTo: dateTo, points: NSNumber(integerLiteral: 30), block: { (list) in
            self.balanceList = list
            block(list)
        }) { (str) in
            blockError(str);
        }
    }
    
    func getChangeUSD()->Double{
        if self.balanceList.count > 0{
            return self.balanceList[1].totalUSD - self.balanceList[0].totalUSD
        } else {
            return 0.0
        }
    }
    
    func getChangePercent()->Double{
        if self.balanceList.count>0{
            return  getChangeUSD() * 100 / self.balanceList.last!.totalUSD
        }
        return 0.0
    }
    
    
    func getTokenAllSearch(_ text: String?) -> List<RWToken> {
        if (text == "") {
            return self.allTokens
        } else {
            let filt = self.allTokens.filter(NSPredicate(format: "name CONTAINS[cd] %@ || symbol CONTAINS[cd] %@", text!, text!))
            let arr:List<RWToken> = List.init()
            if((filt) != nil){
            for token in filt {
                arr.append(token)
            }
            return arr;
            } else {
                return List.init()
            }
            /*var filt: RLMResults? = allTokens().objects(with: NSPredicate(format: "name CONTAINS[cd] %@ || symbol CONTAINS[cd] %@", text, text))
            var arr = [AnyHashable]()
           
            return arr*/
        }

    }
    
    func updateBalanceAll()->Void{
        for token:RWToken in self.tokens {
            token.updateBalabce()
        }
    }
    
    func sortNumberReset() {
        if(self.tokens.count>0){
        let realm = try! Realm()
        let arrNew:List<RWToken> = List.init()
        print("tokens = %f",tokens.count)
        let result = self.tokens.sorted(byKeyPath: "sortNumber", ascending: true);
        
        print("result")
        
        for i in 0..<result.count {
            if let anI:RWToken = result[i] {
                arrNew.append(anI)
            }
        }
       // let realm = try! Realm()
        try! realm.write {
            tokens = arrNew
        }
         try! realm.beginWrite()
        for i in 0..<tokens.count {
           //   try! realm.write {
                tokens[i].sortNumber = i
           // }
        }
            
            try! realm.commitWrite()
        print("tokens = %f",tokens)
        }
        //NSLog
    }
    
    func getSortTokens()->List<RWToken>{
         let arrNew:List<RWToken> = List.init()
        if(self.tokens.count>0){
       // let arrNew:List<RWToken> = List.init()
        let result = self.tokens.sorted(byKeyPath: "sortNumber", ascending: true);
        
        for i in 0..<result.count {
            if let anI:RWToken = result[i] {
                arrNew.append(anI)
            }
        }
        return arrNew;
        } else {
            return arrNew
        }
    }

    func getAllIndexPath() -> [Any]? {
        var arr: [AnyHashable] = []
        for i in 0..<tokens.count {
            arr.append(IndexPath(row: i, section: 0))
        }
        return arr
    }

    
    func getEthSum(_ online: Bool) -> Double {
        let realm = try! Realm()
        let rateOnline = realm.objects(RWRate.self).filter(NSPredicate(format: "tokenSymbol == %@ && online == %@", "ETH", NSNumber(value: online))).first
        if (rateOnline != nil) {
            //print(getFeatSum(online))
            //print(rateOnline?.rate.doubleValue)
            return getFeatSum(online) / (rateOnline?.rate.doubleValue)!
        } else {
            return 0.0;
        }
    }
    
    func getFeatSum(_ online: Bool) -> Double {
        var sum: Double = 0
        for token in self.tokens {
            sum = sum + (token.getInCurrencyDouble(online))
        }
        return sum
    }
    
    func getChangeSum() -> String? {
        let sum = getFeatSum(false)
        let sumOnline = getFeatSum(true)
        let changerate: Double = sumOnline - sum
        return String(format: "%.2f$", changerate)
    }
    
    func getChangeRatePositiv() -> Bool {
        let sum = getFeatSum(false)
        let sumOnline = getFeatSum(true)
        let changerate: Double = sum - sumOnline
        if changerate < 0 {
            return true
        } else {
            return false
        }
    }
    
    func getChangePercent() -> String? {
        let sum = getFeatSum(false)
        let sumOnline = getFeatSum(true)
        let changerate: Double = sum - sumOnline
        var minus: Double = 100 * changerate / sumOnline
        if minus < 0 {
            minus = minus + (minus * -2)
        }
        if sumOnline != 0.0 {
            return String(format:"%.0f%@",minus,"%")
        } else {
            return "..."
        }
    }

}
