//
//  RWTokenDetailVC.swift
//  Rocket Wallet Swift
//
//  Created by Иван Петрашко on 02.08.2018.
//  Copyright © 2018 Иван§ Петрашко. All rights reserved.
//

import UIKit
import Charts
import RealmSwift

class RWTokenDetailVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    

    
    var numbers : [Double] = []
    @IBOutlet weak var table: UITableView!
    var token: RWToken!
    var wallet: RWWallet!
    var periodGraphOnline = 0
    
    var change24dol = 0.0
    var change24per = 0.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        table.register(UINib(nibName: "Cell", bundle: nil), forCellReuseIdentifier: "CellHeader")
        table.register(UINib(nibName: "CellGraph", bundle: nil), forCellReuseIdentifier: "CellGraph")
        table.register(UINib(nibName: "CellTransaction", bundle: nil), forCellReuseIdentifier: "CellTransaction")
        table.delegate = self
        table.dataSource = self
        
         NotificationCenter.default.addObserver(self, selector: #selector(self.setPeriodOnline(notif:)), name: NSNotification.Name("setPeriodOnline"), object: nil)
        
        // Do any additional setup after loading the view.
        if (self.wallet) != nil {
            self.title = "Wallet Info"
            
            self.wallet.getTransaction(block: { (list) in
                self.table.reloadData()
            }) { (str) in
                
            }
        } else {
            self.title = String(format: "%@ Info", self.token.symbol)
            
            token.getTransaction(block: { (list) in
                self.table.reloadData()
            }) { (str) in
                
            }
        }
         self.getBalanceList()
        /*
        RWBalance.getBalance(tokens: RWWallet.getWallet().tokens, date: Date(), block: { (balance:RWBalance) in
            
        }) { (str) in
            
        }
        */
        self.navigationItem.rightBarButtonItem = UIBarButtonItem.init(title: "Send", style: .done, target: self, action: #selector(sendCoin))
    }
    
    @objc func setPeriodOnline(notif:NSNotification){
        self.periodGraphOnline = notif.object as! Int
        self.getBalanceList()
    }
    
    func setChange24h(){
        if (self.wallet) != nil {
            change24dol = self.wallet.getChangeUSD()
            change24per = self.wallet.getChangePercent()
        } else {
            change24dol = self.token.getChangeUSD()
            change24per = self.token.getChangePercent()
        }
    }
    
    func getBalanceList(){
        if (self.wallet) != nil {
            self.wallet.getBalanceList(dateFrom: Date().getDateToPeriod(period: self.periodGraphOnline)!, dateTo: Date(), block: { (list) in
                if self.periodGraphOnline == 0{
                    self.setChange24h()
                }
                self.table.reloadData()
            }) { (str) in
                
            }
        } else {
            token.getBalanceList(dateFrom: Date().getDateToPeriod(period: self.periodGraphOnline)!, dateTo: Date(), block: { (list) in
                if self.periodGraphOnline == 0{
                     self.setChange24h()
                }
                self.table.reloadData()
            }) { (str) in
                
            }
        }
    }
    
    @objc func sendCoin(){
        let tokenSend:RWToken
        if wallet != nil {
            tokenSend = wallet.getSortTokens().first!
        } else {
            tokenSend = token
        }
        
        let portfolio = UIStoryboard(name: "CreateBill", bundle: nil).instantiateViewController(withIdentifier: "RWCreateBillVC") as! RWCreateBillVC
        portfolio.token = tokenSend
        var navigationFirst: UINavigationController! = nil
        navigationFirst = UINavigationController(rootViewController: portfolio)
        present(navigationFirst, animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section==0 {
            return 1
        } else if section == 1{
            return 1
        } else if section == 2{
            return 1
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "CellHeader", for: indexPath) as! RWCellTokenHeader

            if wallet != nil {
                cell.eth.text = String(format: "ETH %f",self.wallet.getEthSum(true))
                cell.sum.text = String(format: "$ %.2f", self.wallet.getFeatSum(true))
                cell.imageProfile.image = UIImage(named: "icon")
                cell.imageProfile.layer.cornerRadius = 21/2
                cell.imageProfile.layer.masksToBounds = true
            } else {
                cell.eth.text = String(format: "%@ %@",token.symbol,self.token.getBalanceString()!)
                cell.sum.text = self.token.getInCurrencyString()
                cell.imageProfile.image = self.token.logo?.getImage(tableView, andNotification: nil)
            }
            
            cell.sumchange.text = String(format: "%.2f$", change24dol)
            cell.percentchange.text = String(format: "%.0f%@", change24per,"%")
            
            
            if change24per>0 {
                cell.percentchange.textColor = UIColor.rwColorRateGreen()
                cell.arr.image = UIImage(named: "Green-arrow")
            } else {
                cell.percentchange.textColor = UIColor.rwColorRateOrange()
                cell.arr.image = UIImage(named: "Red-arrow")
            }
            
            cell.viewLabel.isHidden = true;
            cell.currency.setButtonStyle(style: .Currency)
            cell.currency.setTitle("USD", for: .normal)
            cell.constraitArrow.constant = 0
            cell.currency.isHidden = true
            cell.accessoryType = .none
            cell.selectionStyle = .none
            return cell;
        } else if indexPath.section == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CellGraph", for: indexPath) as! RWCellTokenGraph
            cell.table.delegate = cell;
            cell.table.dataSource = cell;
            cell.table.register(UINib(nibName: "CellCollection", bundle: nil), forCellWithReuseIdentifier: "CellCollection")
            cell.selectionStyle = .none
            cell.setStyleLabels()
            cell.periodOnline = self.periodGraphOnline
            cell.graph.data = updateGraph()
            if wallet != nil {
                cell.graphInit(indicat: self.wallet.balanceList.count>0)
            } else {
                cell.graphInit(indicat: self.token.balanceList.count>0)
              //  balanceList = self.token.balanceList
            }
           
            //cell.update = false
            return cell;
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CellTransaction", for: indexPath) as! RWCellTransaction
            cell.table.register(UINib(nibName: "CellTransactionSingle", bundle: nil), forCellReuseIdentifier: "CellTransactionSingle")
            if wallet != nil {
                cell.transactions = wallet.transactions
            } else {
                cell.transactions = token.transactions
            }
            cell.delegate = self
            cell.table.delegate = cell;
            cell.table.dataSource = cell;
            cell.table.isScrollEnabled = false;
            cell.table.reloadData()

            cell.selectionStyle = .none
            return cell;
        }
    }
    
    func gotoTransaction(int:Int)->Void{
        let portfolio = UIStoryboard(name: "TransactionDetail", bundle: nil).instantiateViewController(withIdentifier: "TransactionDetailVC") as! TransactionDetailVC
        if wallet != nil {
            portfolio.transaction = wallet.transactions[int]
        } else {
            portfolio.transaction = token.transactions[int]
        }
        
        self.navigationController?.pushViewController(portfolio, animated: true)
    }
    
    func updateGraph() -> ChartData{
        var lineChartEntry  = [ChartDataEntry]() //this is the Array that will eventually be displayed on the graph.
        var balanceList:List<RWBalance>
        if wallet != nil {
            balanceList = self.wallet.balanceList
        } else {
            balanceList = self.token.balanceList
        }
        for i in 0..<balanceList.count {
            
            let value = ChartDataEntry(x: Double(i), y: balanceList[i].totalUSD) // here we set the X and Y status in a data chart entry
            value.data = balanceList[i]
            lineChartEntry.append(value) // here we add it to the data set
        }
        
        let set1 = LineChartDataSet(values: lineChartEntry, label: "")
        set1.drawIconsEnabled = false
        set1.setColor(.black)
        set1.setCircleColor(.black)
        set1.lineWidth = 0
        set1.circleRadius = 0
        set1.drawCircleHoleEnabled = false
        set1.drawValuesEnabled = false;
        set1.valueFont = .systemFont(ofSize: 9)
        set1.form = .circle
        set1.formLineWidth = 0
        set1.axisDependency = .right
        set1.mode = .cubicBezier
        
        let gradientColors = [ChartColorTemplates.colorFromString("#ffffff").withAlphaComponent(0.5).cgColor,
                              ChartColorTemplates.colorFromString("#09CAEA").withAlphaComponent(0.5).cgColor]
        let gradient = CGGradient(colorsSpace: nil, colors: gradientColors as CFArray, locations: nil)!
        
        set1.fillAlpha = 1
        set1.fill = Fill(linearGradient: gradient, angle: 90) //.linearGradient(gradient, angle: 90)
        set1.drawFilledEnabled = true
        
        set1.highlightLineWidth = 0.5
        set1.highlightColor = UIColor(red:0, green:0.78, blue:0.91, alpha:1)
        let data = LineChartData(dataSet: set1)
       
        return data
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 0{
            return nil
        } else {
            let view = UIView.init(frame: CGRect.init(x: 0, y: 0, width: self.view.frame.width, height: 34))
            let label = UILabel.init(frame: CGRect.init(x: 16, y: 0, width: 200, height: 34))
            label.font = UIFont.setFontStyle(style: .SemiLight, size: 21)
            label.textColor = UIColor.darkGray
            if(section == 1){
                label.text = "Stats"
            } else if section == 2 {
                 label.text = "Transaction History"
            }
            view.addSubview(label)
             return view
        }
      
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section==0 {
            return 0
        } else {
            return 45
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0{
            return 89
        } else if indexPath.section == 1 {
            return 250
        } else if indexPath.section == 2 {
            if wallet != nil {
                  return CGFloat(self.wallet.transactions.count * 42)
            } else {
                  return CGFloat(self.token.transactions.count * 42)
            }
          
        } else {
            return 0
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
/*
extension RWTokenDetailVC: IAxisValueFormatter {
    func stringForValue(_ value: Double, axis: AxisBase?) -> String {
        return months[Int(value) % months.count]
    }
}*/
