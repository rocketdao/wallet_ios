//
//  RWPortfolioVC.swift
//  Rocket Wallet Swift
//
//  Created by Иван Петрашко on 20.07.2018.
//  Copyright © 2018 Иван§ Петрашко. All rights reserved.
//

import UIKit
import Realm
import RealmSwift
import SwipeCellKit

class RWPortfolioVC: UIViewController,UITableViewDelegate,UITableViewDataSource,SwipeTableViewCellDelegate
{
    
    @IBOutlet weak var loadViewData: UIView!
    
    //var wallet: RWRWWallet.getWallet()
    @IBOutlet weak var table: UITableView!
    @IBOutlet weak var tableHeader: UITableView!
    @IBOutlet weak var viewLabel: UIView!
    @IBOutlet weak var viewToolbar: NSLayoutConstraint!
    
    var loadView:RWLoadViewVC?
    
    var privateKey = ""
      // NSMutableArray<RWToken *> *arrSelected;
    var arrSelected:Array<RWToken>!

    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Portfolio"
        navigationController?.navigationBar.navigationStyle()
        view.backgroundColor = UIColor.rwColorGrayBackground()
        table.dataSource = self as UITableViewDataSource
        table.delegate = self as UITableViewDelegate
        
        tableHeader.register(UINib(nibName: "Cell", bundle: nil), forCellReuseIdentifier: "CellHeader")
        tableHeader.delegate = self
        tableHeader.dataSource = self
        doneStyle()
        //self.loadViewIn.isHidden = true;
        loadViewData.isHidden = true
      //  imageProfile.image = UIImage(named: "ic_launcher-web")
        if (RWWallet.getWallet().tokens.count==0) {
            self.loadViewPresent(open: true)
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == self.table{
        if (RWWallet.getWallet().getSortTokens().count != nil) {
        //    print("tokens = ", RWWallet.getWallet()!);
            return RWWallet.getWallet().getSortTokens().count
        } else {
            return 0
        }
        } else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
        guard orientation == .left else { return nil }
        
        let deleteAction = SwipeAction(style: .default, title: "Send") { action, indexPath in
            // handle action by updating model with deletion
            self.createBill(token: RWWallet.getWallet().getSortTokens()[indexPath.row])
        }
        
        // customize the action appearance
        //deleteAction.image = UIImage(named: "delete")
        deleteAction.backgroundColor = UIColor.rwColorRateGreen()
        return [deleteAction]
    }
    
    func tableView(_ tableView: UITableView, editActionsOptionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> SwipeOptions {
        var options = SwipeOptions()
        options.expansionStyle = .fill
        options.transitionStyle = .border
        return options
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
         if tableView == self.table{
            return 64
         } else {
            return 87
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == self.table{
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as! RWCellToken
        cell.delegate = self
        cell.name.text = RWWallet.getWallet().getSortTokens()[indexPath.row].name
        cell.count.text = RWWallet.getWallet().getSortTokens()[indexPath.row].getBalanceString()
        cell.conversion.text = RWWallet.getWallet().getSortTokens()[indexPath.row].getInCurrencyString()
            
        //cell.rate
        cell.rate.text = RWWallet.getWallet().getSortTokens()[indexPath.row].getRateInString()
        cell.changerate.text = String(format: "%.0f%@", RWWallet.getWallet().getSortTokens()[indexPath.row].change24h.doubleValue,"%")
        if RWWallet.getWallet().getSortTokens()[indexPath.row].change24h.doubleValue >= 0 {
            cell.changerate.textColor = UIColor.rwColorRateGreen()
             cell.arr.image = UIImage(named: "Green-arrow")
        } else {
            cell.changerate.textColor = UIColor.rwColorRateOrange()
            cell.arr.image = UIImage(named: "Red-arrow")
        }
    
        cell.tintColor = UIColor.rwColorBlueButton()
        // cell.logo.layer.cornerRadius = 39/2;
        // cell.logo.layer.masksToBounds = true;
        if table.isEditing {
            cell.count.isHidden = true
            cell.conversion.isHidden = true
        } else {
            cell.count.isHidden = false
            cell.conversion.isHidden = false
        }
        cell.logo.image = RWWallet.getWallet().getSortTokens()[indexPath.row].logo?.getImage(tableView, andNotification: nil)
            
        cell.setStyle()
        
        return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CellHeader") as! RWCellTokenHeader
            if(RWWallet.getWallet().tokens.count>0){
                cell.eth.text = String(format: "ETH %f", (RWWallet.getWallet().getEthSum(true)))
                cell.sum.text = String(format: "$ %.2f", (RWWallet.getWallet().getFeatSum(true)))
                cell.sumchange.text = String(format: "%.2f%@",RWWallet.getWallet().change24hSum.doubleValue,"$")
                cell.percentchange.text = String(format: "%.0f%@",RWWallet.getWallet().change24hPercent.doubleValue,"%")
                if RWWallet.getWallet().change24hSum.doubleValue >= 0{
                    cell.percentchange.textColor = UIColor.rwColorRateGreen()
                    cell.arr.image = UIImage(named: "Green-arrow")
                } else {
                    cell.percentchange.textColor = UIColor.rwColorRateOrange()
                    cell.arr.image = UIImage(named: "Red-arrow")
                }
                cell.imageProfile.image = UIImage(named: "icon")
                cell.imageProfile.layer.cornerRadius = 21/2
                cell.imageProfile.layer.masksToBounds = true
            }
            
            cell.currency.isHidden = true
            
            if(self.table.isEditing){
                cell.viewLabel.isHidden = false
            } else {
                cell.viewLabel.isHidden = true
            }
            return cell
        }
    }
    
    func updateLabelSum() {
        if(RWWallet.getWallet() != nil){
            self.tableHeader.reloadData()
        }
    }
    func updateBalance() {
         if (RWWallet.getWallet().getSortTokens().count>0) {
            RWWallet.getWallet().updateBalanceAll()
        }
    }
    
    func loadViewPresent(open:Bool){
        if open {
            loadViewData.isHidden = false
            loadView = UIStoryboard(name: "Portfolio", bundle: nil).instantiateViewController(withIdentifier: "RWLoadViewVC") as! RWLoadViewVC
            present(loadView!, animated: false)
        } else {
            if loadView != nil {
                loadViewData.isHidden = true
                self.loadView?.dismiss(animated: true) {
                 print("true")
                }
            }
        }
    }
    
    func getChange24h(){
        RWWallet.getWallet().getChange24(block: { (change) in
            self.tableHeader.reloadData()
            self.table.reloadData()
        }) { (str) in
            
        }
    }
    
    func updateRates() {
        if (RWWallet.getWallet().tokens.count>0) {
            RWRate.getRatesByServerSave(RWWallet.getWallet().getSortTokens(), andDate: Date(), andBlock: { save in
                self.table.reloadData()
                //self.updateLabelSum()
            }, andError: { string in
                print("ошибка - \(String(describing: string))")
            })
            self.getChange24h()
        } else {
            RWWallet.export(withPrivateKey: privateKey, andBlock: { wallet in
                print(RWWallet.getWallet())
                self.table.reloadData()
                self.updateLabelSum()
                RWRate.getRatesByServerSave(RWWallet.getWallet().getSortTokens(), andDate: Date(), andBlock: { save in
                  //  self.updateLabelSum()
                   self.table.reloadData()
                   self.loadViewPresent(open: false)
                }, andError: { string in
                    print("ошибка - \(string)")
                })
                self.getChange24h()
            }, andError: { string in
                print("ошибка - \(string)")
            })
            
        }
    }
    
   override func viewWillAppear(_ animated: Bool) {
        updateRates()
        table.reloadData()
        updateLabelSum()
        updateBalance();
    }

    

    @objc func editFunc() {
        table.setEditing(true, animated: true)
        self.tableHeader.reloadData()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3, execute: {
            // do work in the UI thread here
            self.table.beginUpdates()
            self.table.reloadRows(at: RWWallet.getWallet().getAllIndexPath() as! [IndexPath], with: .none)
            self.table.endUpdates()
        })
        self.arrSelected = []
        table.allowsSelectionDuringEditing = true
        //viewLabel.isHidden = false
        let done = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(self.doneFunc))
        navigationItem.leftBarButtonItem = done
        let addToken = UIBarButtonItem(title: "Add Token", style: .done, target: self, action: #selector(self.addTokenFunc))
        navigationItem.rightBarButtonItem = addToken
        navigationController?.setToolbarHidden(false, animated: false)
        UIView.animate(withDuration: 0.01, animations: {
            self.tabBarController?.tabBar.isHidden = true
            self.tabBarController?.tabBar.transform = CGAffineTransform(translationX: 0, y: 50)
            self.view.layoutIfNeeded()
        }) { finished in
            self.navigationController?.setToolbarHidden(true, animated: false)
            self.tabBarController?.tabBar.isHidden = true
            self.viewToolbar.constant = 48
        }
    }
    
    @objc func doneFunc() {
        UIView.animate(withDuration: 0.1, animations: {
            self.tabBarController?.tabBar.isHidden = false
            self.tabBarController?.tabBar.transform = .identity
        })
        table.setEditing(false, animated: true)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3 , execute: {
            // do work in the UI thread here
            self.table.beginUpdates()
            self.table.reloadRows(at: RWWallet.getWallet().getAllIndexPath() as! [IndexPath], with: .none)
            self.table.endUpdates()
        })
        //  [self.view layoutIfNeeded];
        table.allowsSelectionDuringEditing = false
        // [self.table reloadData];
        doneStyle()
    }

    
    @objc func addTokenFunc() {
        let portfolio = UIStoryboard(name: "Portfolio", bundle: nil).instantiateViewController(withIdentifier: "RWTokenListVC") as! RWTokenListVC
        portfolio.wallet = RWWallet.getWallet()
        var navigationFirst: UINavigationController! = nil
        navigationFirst = UINavigationController(rootViewController: portfolio)
        present(navigationFirst, animated: true)
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
        return UITableViewCellEditingStyle(rawValue: 3)!
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.setNeedsLayout()
        if tableView.isEditing {
            arrSelected.append((RWWallet.getWallet().getSortTokens()[indexPath.row]))
           //print(arrSelected)
        } else {
             if tableView == self.table{
                let portfolio = UIStoryboard(name: "Portfolio", bundle: nil).instantiateViewController(withIdentifier: "RWTokenDetailVC") as! RWTokenDetailVC
                portfolio.token = RWWallet.getWallet().getSortTokens()[indexPath.row]
                self.navigationController?.pushViewController(portfolio, animated: true)
                
            } else {
                let portfolio = UIStoryboard(name: "Portfolio", bundle: nil).instantiateViewController(withIdentifier: "RWTokenDetailVC") as! RWTokenDetailVC
                portfolio.wallet = RWWallet.getWallet()
                self.navigationController?.pushViewController(portfolio, animated: true)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        if tableView.isEditing {
             let news = arrSelected.filter({$0.symbol == RWWallet.getWallet().getSortTokens()[indexPath.row].symbol})
            if news.count>0{
                if let anObject = news.first {
                    while let elementIndex = arrSelected.index(of: anObject) { arrSelected.remove(at: elementIndex) }
                }
            }
        } else {
            
        }
    }

    
    func tableView(_ tableView: UITableView, targetIndexPathForMoveFromRowAt sourceIndexPath: IndexPath, toProposedIndexPath proposedDestinationIndexPath: IndexPath) -> IndexPath {
        return proposedDestinationIndexPath
    }
    
    func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
       
        let tokens = RWWallet.getWallet().getSortTokens()
       // print(String(format:"tokens1 = %@",tokens))
        let realm = try! Realm()
        try! realm.write {
            let token = tokens[sourceIndexPath.row]
            tokens.remove(at: sourceIndexPath.row)
            tokens.insert(token, at: destinationIndexPath.row)
        }
        try! realm.write {
            for i in 0..<tokens.count {
                tokens[i].sortNumber = i
            }
        }
       // table.reloadData()
    }

    
    func doneStyle() {
        let edit = UIBarButtonItem(title: "Edit", style: .done, target: self, action: #selector(self.editFunc))
        navigationItem.leftBarButtonItem = edit
        
        let send = UIBarButtonItem(title: "Send", style: .done, target: self, action: #selector(self.createBillFirst))
        navigationItem.rightBarButtonItem = send
        //viewLabel.isHidden = true
        self.tableHeader.reloadData()
        viewToolbar.constant = 0
    }
    
    @objc func createBillFirst() -> Void {
        createBill(token: RWWallet.getWallet().getSortTokens().first!)
    }
    
    func createBill(token:RWToken) -> Void {
        let portfolio = UIStoryboard(name: "CreateBill", bundle: nil).instantiateViewController(withIdentifier: "RWCreateBillVC") as! RWCreateBillVC
        portfolio.token = token
        var navigationFirst: UINavigationController! = nil
        navigationFirst = UINavigationController(rootViewController: portfolio)
        present(navigationFirst, animated: true)
    }
    
    @IBAction func removeAll(_ sender: Any) {
        trashFunc()
    }
    
    func trashFunc() {
        var arrRemoveIndex: [IndexPath] = []
        for i in 0..<arrSelected.count {
            print(String(format: "selected = %@",arrSelected[i]))
            arrRemoveIndex.append(IndexPath(row: (RWWallet.getWallet().getSortTokens().index(matching: NSPredicate(format: "symbol == %@", arrSelected[i].symbol)))!, section: 0))
            let realm = try! Realm()
            try! realm.write {
                RWWallet.getWallet().tokens.remove(at: RWWallet.getWallet().tokens.index(matching: NSPredicate(format: "symbol == %@", arrSelected[i].symbol))!)
              //  var token =  RWWallet.getWallet().getSortTokens()[RWWallet.getWallet().getSortTokens().index(matching: NSPredicate(format: "symbol == %@", arrSelected[i].symbol))!]
               // print(String(format: "str = %@", token))
                //RWWallet.getWallet().getSortTokens()
                //realm.delete(token)
                //RWWallet.getWallet().getSortTokens().
            }
        }
        
        print(String(format: "arr = %@",RWWallet.getWallet().getSortTokens()))
       // RWWallet.getWallet().sortNumberReset()
        arrSelected = []
        table.beginUpdates()
        table.deleteRows(at: arrRemoveIndex , with: .top)
        table.endUpdates()
        doneFunc()
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
