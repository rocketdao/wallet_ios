//
//  RWImportWalletVC.swift
//  Rocket Wallet Swift
//
//  Created by Иван Петрашко on 17.08.2018.
//  Copyright © 2018 Иван§ Петрашко. All rights reserved.
//

import UIKit
import AVFoundation
import QRCodeReader
import Web3
import Keystore
import CryptoSwift
import EthereumKit

enum ImportSegmentStap:Int {
    case Phase
    case Keystore
    case Private
    case QR
}

class RWImportWalletVC: UIViewController, QRCodeReaderViewControllerDelegate,UITextViewDelegate {
    @IBOutlet weak var previewQR: QRCodeReaderView!{
        didSet {
            previewQR.setupComponents(showCancelButton: false, showSwitchCameraButton: false, showTorchButton: false, showOverlayView: true, reader: reader)
        }
    }
    lazy var reader: QRCodeReader = QRCodeReader()
    lazy var readerVC: QRCodeReaderViewController = {
        let builder = QRCodeReaderViewControllerBuilder {
            $0.reader                  = QRCodeReader(metadataObjectTypes: [.qr], captureDevicePosition: .back)
            $0.showTorchButton         = true
            $0.preferredStatusBarStyle = .lightContent
            
            $0.reader.stopScanningWhenCodeIsFound = false
        }
        
        return QRCodeReaderViewController(builder: builder)
    }()

    @IBOutlet weak var field: UITextField!
    @IBOutlet weak var textview: UITextView!
    @IBOutlet weak var segment: UISegmentedControl!
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Import Wallet"
        navigationController?.navigationBar.navigationStyle()
        self.textview.layer.borderWidth = 1
        self.textview.layer.borderColor = UIColor(red:0.8, green:0.8, blue:0.8, alpha:0.3).cgColor
        self.textview.delegate = self
        self.textview.font = UIFont.setFontStyle(style: .Regular, size: 15)
        
        self.field.layer.borderWidth = 1
        self.field.layer.borderColor = UIColor(red:0.8, green:0.8, blue:0.8, alpha:0.3).cgColor
        self.field.setLeftPaddingPoints(5)
        self.field.attributedPlaceholder = NSAttributedString(string: "Password",
                                                               attributes: [NSAttributedStringKey.foregroundColor: UIColor.lightGray])
        self.field.font = UIFont.setFontStyle(style: .Regular, size: 15)
        // Do any additional setup after loading the view.
        
        self.segment.tintColor = UIColor.rwColorBlueButton()
        self.segment.addTarget(self, action: #selector(self.changeSegment(segment:)), for: .valueChanged)
        
        
        
        let addToken = UIBarButtonItem(title: "Import", style: .done, target: self, action: #selector(self.importWallet))
        navigationItem.rightBarButtonItem = addToken
        let cancel = UIBarButtonItem(title: "Cancel", style: .done, target: self, action: #selector(self.cancelFunc))
        navigationItem.leftBarButtonItem = cancel
        self.segmentStap(stap: ImportSegmentStap(rawValue: 0)!)
        
    }
    
    private func checkScanPermissions() -> Bool {
        do {
            return try QRCodeReader.supportsMetadataObjectTypes()
        } catch let error as NSError {
            let alert: UIAlertController
            
            switch error.code {
            case -11852:
                alert = UIAlertController(title: "Error", message: "This app is not authorized to use Back Camera.", preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: "Setting", style: .default, handler: { (_) in
                    DispatchQueue.main.async {
                        if let settingsURL = URL(string: UIApplicationOpenSettingsURLString) {
                            UIApplication.shared.openURL(settingsURL)
                        }
                    }
                }))
                
                alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            default:
                alert = UIAlertController(title: "Error", message: "Reader not supported by the current device", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
            }
            
            present(alert, animated: true, completion: nil)
            
            return false
        }
    }
    
    func getPrivateByString(str:String){
       /* let priv = try? EthereumPrivateKey(
            hexPrivateKey: str
        ).publicKey*/
        let tabBar = RWTabBar()
        //or the homeController
        tabBar.privatekey = str
        tabBar.setVC()
        present(tabBar, animated: true)
    }
    
    func scanInPreviewAction() {
        guard checkScanPermissions(), !reader.isRunning else { return }
        
        reader.didFindCode = { result in
           
            self.segmentStap(stap: .Private)
             self.textview.text = result.value
            self.textview.textColor = UIColor.black
            self.segment.selectedSegmentIndex = 2
          //  print("Completion with result: \(result.value) of type \(result.metadataType)")
        }
        
        reader.startScanning()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @objc func importWallet()->Void{
        switch ImportSegmentStap(rawValue: segment.selectedSegmentIndex)! {
        case .Phase:
            do {
                let mnemonic = self.textview.text
                    .split(separator: " ")
                    .map(String.init)
                let seed = try Mnemonic.createSeed(mnemonic: mnemonic)
                let wallet = HDWallet(seed: seed, network: .main)
                
                do {
                     let privateKey = try wallet.generatePrivateKey(at: 0)
                        self.getPrivateByString(str: privateKey.raw.toHexString())
                } catch let error {
                    print("Error: \(error)")
                    let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
            } catch {
                print(error)
                let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
            break
        case .Keystore:
            let decoder = JSONDecoder()
            
            let keystoreData: Data = self.textview.text.data(using: .utf8)!
            do {
            let keystore = try decoder.decode(Keystore.self, from: keystoreData)
            
            let password = self.field.text
            let privateKey = try keystore.privateKey(password: password!)
                
            let privateNew = try EthereumPrivateKey(bytes: privateKey)
           // let privateNew = try EthereumPrivateKey(
            print(privateNew.hex())    // Your decrypted private key
                self.getPrivateByString(str: privateNew.hex())
            } catch {
                let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
            break
        case .Private:
            do {
               let key = try EthereumPrivateKey(hexPrivateKey: self.textview.text)
                self.getPrivateByString(str: self.textview.text)
            } catch {
                let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
            //let privateKey = PrivateKey.
            
           
          //  self.getPrivateByString(str: "7743b7ddc437246c4c3cf81ae0d2b1299a4011fa7f67aec40bd9841d1d12544b")
            
            break
        case .QR: break
            
        }
        
    }
    
    @objc func cancelFunc() {
        navigationController?.dismiss(animated: true)
    }

    
    @objc func changeSegment(segment:UISegmentedControl)->Void{
        self.segmentStap(stap: ImportSegmentStap(rawValue: segment.selectedSegmentIndex)!)
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            switch ImportSegmentStap(rawValue: segment.selectedSegmentIndex)! {
            case .Phase:
                textView.text = "12 or 24 words separated by blank spaces"
                break
            case .Keystore:
                textView.text = "Keystone"
                break
            case .Private:
                textView.text = "Private key"
                break
            case .QR:
                break
            }
            textView.textColor = UIColor.lightGray
        }
    }
    
    func segmentStap(stap:ImportSegmentStap)->Void{
         self.view.endEditing(true)
        switch stap {
        case .Phase:
           
            self.textview.isHidden = false
            self.field.isHidden = true
            self.previewQR.isHidden = true
            self.textview.text = "12 or 24 words separated by blank spaces"
            self.textview.textColor = UIColor.lightGray
            reader.stopScanning()
            break
        case .Keystore:
            self.textview.isHidden = false
            self.field.isHidden = false
            self.previewQR.isHidden = true
            self.textview.text = "Keystone"
            self.textview.textColor = UIColor.lightGray
            reader.stopScanning()
            break
        case .Private:
            self.textview.isHidden = false
            self.field.isHidden = true
            self.previewQR.isHidden = true
            self.textview.text = "Private key"
            self.textview.textColor = UIColor.lightGray
            reader.stopScanning()
            break
        case .QR:
            self.textview.isHidden = true
            self.field.isHidden = true
            self.previewQR.isHidden = false
            self.scanInPreviewAction()
            break
    }
    }
    
    // MARK: - QRCodeReader Delegate Methods
    
    func reader(_ reader: QRCodeReaderViewController, didScanResult result: QRCodeReaderResult) {
        reader.stopScanning()
        
        dismiss(animated: true) { [weak self] in
            let alert = UIAlertController(
                title: "QRCodeReader",
                message: String (format:"%@ (of type %@)", result.value, result.metadataType),
                preferredStyle: .alert
            )
            alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
            
            self?.present(alert, animated: true, completion: nil)
        }
    }
    
    func reader(_ reader: QRCodeReaderViewController, didSwitchCamera newCaptureDevice: AVCaptureDeviceInput) {
        print("Switching capturing to: \(newCaptureDevice.device.localizedName)")
    }
    
    func readerDidCancel(_ reader: QRCodeReaderViewController) {
        reader.stopScanning()
        
        dismiss(animated: true, completion: nil)
    }
 

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
