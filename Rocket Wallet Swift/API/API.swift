//
//  API.swift
//  Rocket Wallet Swift
//
//  Created by Иван Петрашко on 20.07.2018.
//  Copyright © 2018 Иван§ Петрашко. All rights reserved.
//

import UIKit
import RealmSwift

class API: NSObject {
    
        override init() {
            super.init()
        }
        static let sharedInstance: API = {
            let instance = API()
            return instance
        }()
    
    var jsonBalance = "[{\"address\":\"0xd2301b372bac746c493378368117f0aee784c939\",\"decimals\":18,\"name\":\"VTR\",\"symbol\":\"VTR\",\"balance\":333},{\"address\":\" \",\"decimals\":18,\"name\":\"ETH\",\"symbol\":\"ETH\",\"balance\":2}]"
    
    var jsonRate = "{\"currency\":\"USD\",\"rates\":[{\"rate\":0.00302857,\"tokenSymbol\":\"VTR\"},{\"rate\":298.548,\"tokenSymbol\":\"ETH\"}],\"date\":\"2018-08-20T08:01:00.000Z\"}"
    
    func request(url:String, completion:@escaping (Any)->Void, errorBlock:@escaping (String)->Void)->Void {
        print(String(format: "%@%@", Constants.SERVER_URL,url))
        let request = NSMutableURLRequest(url: NSURL(string: String(format: "%@%@", Constants.SERVER_URL,url))! as URL)
        if Constants.KOVAN {
        if url.substr(0, 5) == "rates" {
            let dict = try? JSONSerialization.jsonObject(with: self.jsonRate.data(using: .utf8)!, options: [])
                if((dict) != nil){
                //   print(dict)
                    DispatchQueue.main.async {
                        completion(dict)
                        return
                    }
            }
        } else if url.substr(0, 5) == "token" && url.substr(0, 9) != "tokens/ra" {
                let dict = try? JSONSerialization.jsonObject(with: self.jsonBalance.data(using: .utf8)!, options: [])
                if((dict) != nil){
                    //   print(dict)
                    DispatchQueue.main.async {
                        completion(dict)
                        return
                    }
                }
        } else {
            completion("")
        }
        } else {
        let session = URLSession.shared
        request.httpMethod = "GET"
        
        //if(request.url?.absoluteString.substr(, <#T##length: Int##Int#>))
        print(url)
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: {data, response, error -> Void in
            //print("Response: \(response)")
            if(error == nil){
                if data != nil {
                    let dict = try? JSONSerialization.jsonObject(with: data!, options: [])
                    if((dict) != nil){
                        print(dict)
                        DispatchQueue.main.async {
                            completion(dict)
                            return
                        }
                    }
                    errorBlock("Ошибка")
                } else {
                    errorBlock("Ошибка")
                }
            } else {
                errorBlock((error?.localizedDescription)!)
            }
        })
        
        task.resume()
        }
    }
    
    
    func getTokenList() -> List<RWToken> {
        var filePath: String
        filePath = Bundle.main.path(forResource: "tokens-eth", ofType: "json") ?? ""
        let myJSON = try? String(contentsOfFile: filePath, encoding: .utf8)
        var error: Error? = nil
        var jsonDataArray: [NSDictionary]? = nil
        if let anEncoding = myJSON?.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue)) {
            jsonDataArray = try! JSONSerialization.jsonObject(with: anEncoding, options: []) as? [Any] as! [NSDictionary]
        }
        let arr:List<RWToken> = List.init()
     //   let arr = RLMArray(objectClassName: "RWToken")
      //  for dict: [AnyHashable : Any]? in jsonDataArray as? [[AnyHashable : Any]?] ?? [[AnyHashable : Any]?]() {
        for dict:NSDictionary in jsonDataArray!{
            let token = RWToken()
            token.symbol = (dict["symbol"] as! String).uppercased()
            token.address = dict["address"] as! String
            token.decimals = dict["decimals"] as! Int
            token.name = dict["name"] as! String
            
            token.logo = RWImage()
            
            let dict2 = dict["logo"] as! NSDictionary
           // urls = dict2["src"] as! String
            //logo = RWImage()
            //logo?.url = urls
          //  print(urls)
            token.logo?.url = dict2["src"] as! String
            print("url logo = %@",token.logo?.url ?? "");
            arr.append(token)
        }
        return arr
    }


}
