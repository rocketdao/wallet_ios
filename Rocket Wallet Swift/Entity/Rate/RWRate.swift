//
//  RWRate.swift
//  Rocket Wallet Swift
//
//  Created by Иван Петрашко on 20.07.2018.
//  Copyright © 2018 Иван§ Петрашко. All rights reserved.
//

import UIKit
import Realm
import RealmSwift

class RWRate: Object {

    @objc dynamic var tokenSymbol = ""
    @objc dynamic var rate: NSNumber = 0.0
    @objc dynamic var online = false
    @objc dynamic var date: Date?

    class func getRatesByServer(_ tokens: List<RWToken>?, andDate date: Date!, andOnline online: Bool, andBlock block: @escaping (_ save: Bool) -> Void, andError blockError: @escaping (String) -> Void)->Void {
        var tokenString = ""
        for token: RWToken in tokens! {
            tokenString = "\(tokenString)&tokenTypeList=\(token.symbol)"
        }
        
        API.sharedInstance.request(url: "rates-by-date?date=\(date.getDateString(byFormat: Constants.DATETIME_SERVER_FORMAT) ?? "")\(tokenString)", completion: { (json:Any) in
            let realm = try! Realm()
            let arr = json as? NSDictionary
            if arr != nil{
            for dict in arr!["rates"] as! [NSDictionary] {
                RWRate.removeAllRate(byToken: dict.object(forKey: "tokenSymbol") as? String, andOnline: online)
                let rate = RWRate()
                rate.rate = dict.object(forKey: "rate") as! NSNumber
                rate.tokenSymbol = (dict.object(forKey: "tokenSymbol") as! String).uppercased()
             //   print("rate = \(rate.rate)")
                rate.online = online
              //  rate.date = Date(fromServerString: dict.object(forKey: "date"))
               // rate.date = Date.getFromServer(str: dict.object(forKey: "date") as! String)
                try! realm.write {
                    realm.add(rate)
                }
            }
                if Constants.KOVAN {
                DispatchQueue.main.asyncAfter(deadline: .now() + 2) { // change 2 to desired number of seconds
                    // Your code with delay
                    block(true)
                    } } else {
                    block(true)
                }
            }

        }) { (str:String) in
            blockError(str)
        }

    }
    
    class func removeAllRate(byToken token: String?, andOnline online: Bool) {
       let realm = try! Realm()
        try! realm.write {
            //print(realm.objects(RWRate.self).filter(NSPredicate(format: "tokenSymbol == %@ && online == %@", token!.uppercased(), NSNumber(value: online))))
           
            realm.delete(realm.objects(RWRate.self).filter(NSPredicate(format: "tokenSymbol == %@ && online == %@", token!.uppercased(), NSNumber(value: online))))
            
            //print(realm.objects(RWRate.self).filter(NSPredicate(format: "tokenSymbol == %@ && online == %@", token!, NSNumber(value: online))))
        }
    }
    
    class func getRatesByServerSave(_ tokens: List<RWToken>?, andDate date: Date?, andBlock block: @escaping (_ save: Bool) -> Void, andError blockError: @escaping (_ string: String?) -> Void) {
        self.getRatesByServer(tokens, andDate: date, andOnline: true, andBlock: { (bool1:Bool) in
            
            block(true)
            
           /* self.getRatesByServer(tokens, andDate: date?.minusDay(), andOnline: false, andBlock: { (bol2:Bool) in
                block(true)
            }, andError: { (str2:String) in
                 blockError(str2)
            })*/
        }) { (str:String) in
             blockError(str)
        }
    }

}
