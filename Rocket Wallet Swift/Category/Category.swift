//
//  Category.swift
//  Rocket Wallet Swift
//
//  Created by Иван Петрашко on 20.07.2018.
//  Copyright © 2018 Иван§ Петрашко. All rights reserved.
//

import Foundation
import UIKit

struct Constants {
    static let SERVER_URL = "https://api.rocketwallet.io/v1/"
    static let DATETIME_SERVER_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
    static let GRAPH_ARRAY_PERIOD = ["1D","1W","1M","3M","6M","1Y","2Y","5Y","10Y"]
    
    static var KOVAN = true
    
    
   static let URL_NODA = "https://kovan.infura.io/fd28b54b765d41c8d352d092576bb125"
   //static let URL_NODA = "https://mainnet.infura.io/yFx7ovyD8etn79WoPTii"
}

extension UIViewController {
    func alert(title:String,message:String){
        let alert = UIAlertController.init(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction.init(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}

extension String {
    func isValidDouble(maxDecimalPlaces: Int) -> Bool {
        // Use NumberFormatter to check if we can turn the string into a number
        // and to get the locale specific decimal separator.
        let formatter = NumberFormatter()
        formatter.allowsFloats = true // Default is true, be explicit anyways
        let decimalSeparator = formatter.decimalSeparator ?? "."  // Gets the locale specific decimal separator. If for some reason there is none we assume "." is used as separator.
        
        // Check if we can create a valid number. (The formatter creates a NSNumber, but
        // every NSNumber is a valid double, so we're good!)
        if formatter.number(from: self) != nil {
            // Split our string at the decimal separator
            let split = self.components(separatedBy: decimalSeparator)
            
            // Depending on whether there was a decimalSeparator we may have one
            // or two parts now. If it is two then the second part is the one after
            // the separator, aka the digits we care about.
            // If there was no separator then the user hasn't entered a decimal
            // number yet and we treat the string as empty, succeeding the check
            let digits = split.count == 2 ? split.last ?? "" : ""
            
            // Finally check if we're <= the allowed digits
            return digits.characters.count <= maxDecimalPlaces    // TODO: Swift 4.0 replace with digits.count, YAY!
        }
        
        return false // couldn't turn string into a valid number
    }
}


extension UIImage {
    class func image(from layer: CALayer) -> UIImage? {
        UIGraphicsBeginImageContextWithOptions(layer.bounds.size,
                                               layer.isOpaque, UIScreen.main.scale)
        
        defer { UIGraphicsEndImageContext() }
        
        // Don't proceed unless we have context
        guard let context = UIGraphicsGetCurrentContext() else {
            return nil
        }
        
        layer.render(in: context)
        return UIGraphicsGetImageFromCurrentImageContext()
    }
}

public protocol DataConvertable {
    static func +(lhs: Data, rhs: Self) -> Data
    static func +=(lhs: inout Data, rhs: Self)
}

extension DataConvertable {
    public static func +(lhs: Data, rhs: Self) -> Data {
        var value = rhs
        let data = Data(buffer: UnsafeBufferPointer(start: &value, count: 1))
        return lhs + data
    }
    
    public static func +=(lhs: inout Data, rhs: Self) {
        lhs = lhs + rhs
    }
}

extension UInt8: DataConvertable {}
extension UInt32: DataConvertable {}

extension String {
    func toData() -> Data {
        return decomposedStringWithCompatibilityMapping.data(using: .utf8)!
    }
}

extension UIColor {
    
    class func rwColorBlueButton() -> UIColor {
        return UIColor(red: 0, green: 0.78, blue: 0.91, alpha: 1)
    }
    
    class func rwColorGrayTextSum() -> UIColor {
        return UIColor(red: 0.35, green: 0.38, blue: 0.44, alpha: 1)
    }
    
    class func rwColorGrayBackground() -> UIColor {
        return UIColor(red: 248.0 / 255.0, green: 248.0 / 255.0, blue: 248.0 / 255.0, alpha: 1)
    }
    
    class func rwColorRateOrange() -> UIColor {
        return UIColor(red: 0.98, green: 0.27, blue: 0, alpha: 1)
    }
    
    class func rwColorRateGreen() -> UIColor {
        return UIColor(red: 0, green: 0.94, blue: 0.51, alpha: 1)
    }
    
    
    class func rwColorGrayGraph() -> UIColor {
        return UIColor(red:0.56, green:0.56, blue:0.58, alpha:1)
    }
    
}

enum RWButtonStyle {
    case Sign
    case Currency
    case SendBill
}

extension UIButton {
    func setButtonStyle(style:RWButtonStyle) -> Void {
        switch style {
        case .Sign:
           // self.setba
            self.backgroundColor = UIColor.rwColorBlueButton()
            self.setTitleColor(UIColor.white, for: .normal)
            break
        case .Currency:
            self.backgroundColor = UIColor.rwColorBlueButton()
            self.setTitleColor(UIColor.white, for: .normal)
            self.titleLabel?.font = UIFont(name: "Stratos-Regular-Desktop", size: 17)
            self.layer.cornerRadius = 12
            self.layer.masksToBounds = true
            break
        case .SendBill:
            self.backgroundColor = UIColor.rwColorBlueButton()
            self.setTitleColor(UIColor.white, for: .normal)
            self.titleLabel?.font = UIFont(name: "Stratos-Regular-Desktop", size: 13)
            self.layer.cornerRadius = 12
            self.layer.masksToBounds = true
            break
        default:
            break
        }
    }
}

enum RWLabelFontStyle {
    case Regular
    case SemiBold
    case SemiLight
}

extension UIFont {
    class func setFontStyle(style:RWLabelFontStyle,size:CGFloat) -> UIFont!{
        switch style {
        case .Regular:
            return UIFont(name: "StratosLC-Regular", size: size)
        case .SemiBold:
            return UIFont(name: "StratosLC-SemiBold", size: size)
        case .SemiLight:
            return UIFont(name: "StratosLC-SemiLight", size: size)
        default:
            return UIFont(name: "StratosLC-SemiBold", size: size)
        }
    }
}

extension Date {
    func getDateString(byFormat format: String?) -> String? {
        let formatter = DateFormatter()
        formatter.locale = NSLocale(localeIdentifier: NSLocale.preferredLanguages[0]) as Locale
        formatter.dateFormat = format ?? ""
        return formatter.string(from: self as Date)
    }
    
    static func serverDateFormatter() -> DateFormatter? {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = NSLocale(localeIdentifier: NSLocale.preferredLanguages[0]) as Locale
        if let aName = NSTimeZone(name: "UTC") {
            dateFormatter.timeZone = aName as TimeZone
        }
        dateFormatter.dateFormat = Constants.DATETIME_SERVER_FORMAT
        return dateFormatter
    }
    static func getFromServer(str:String)->Date{
        let serverDateFormatter = Date.serverDateFormatter()
        let date = serverDateFormatter?.date(from: str)
        return date!
    }
    
    func timeAgoSinceDate(_ date:Date, numericDates:Bool = false) -> String {
        let calendar = NSCalendar.current
        let unitFlags: Set<Calendar.Component> = [.minute, .hour, .day, .weekOfYear, .month, .year, .second]
        let now = Date()
        let earliest = now < date ? now : date
        let latest = (earliest == now) ? date : now
        let components = calendar.dateComponents(unitFlags, from: earliest,  to: latest)
        
        if (components.year! >= 2) {
            return "\(components.year!) years ago"
        } else if (components.year! >= 1){
            if (numericDates){
                return "1 year ago"
            } else {
                return "Last year"
            }
        } else if (components.month! >= 2) {
            return "\(components.month!) months ago"
        } else if (components.month! >= 1){
            if (numericDates){
                return "1 month ago"
            } else {
                return "Last month"
            }
        } else if (components.weekOfYear! >= 2) {
            return "\(components.weekOfYear!) weeks ago"
        } else if (components.weekOfYear! >= 1){
            if (numericDates){
                return "1 week ago"
            } else {
                return "Last week"
            }
        } else if (components.day! >= 2) {
            return "\(components.day!) days ago"
        } else if (components.day! >= 1){
            if (numericDates){
                return "1 day ago"
            } else {
                return "Yesterday"
            }
        } else if (components.hour! >= 2) {
            return "\(components.hour!) hours ago"
        } else if (components.hour! >= 1){
            if (numericDates){
                return "1 hour ago"
            } else {
                return "An hour ago"
            }
        } else if (components.minute! >= 2) {
            return "\(components.minute!) minutes ago"
        } else if (components.minute! >= 1){
            if (numericDates){
                return "1 minute ago"
            } else {
                return "A minute ago"
            }
        } else if (components.second! >= 3) {
            return "\(components.second!) seconds ago"
        } else {
            return "Just now"
        }
        
    }
    
    func minusDay() -> Date? {
        var dayComponent = DateComponents()
        dayComponent.day = -1
        let theCalendar = Calendar.current
        let nextDate:Date? = theCalendar.date(byAdding: dayComponent, to: self)
        return nextDate
    }
    
    func getDateToPeriod(period:Int) -> Date? {
        var dayComponent = DateComponents()
        switch period {
        case 0:
            dayComponent.day = -1
            return Calendar.current.date(byAdding: dayComponent, to: self)
        case 1:
            dayComponent.weekOfYear = -1
            return Calendar.current.date(byAdding: dayComponent, to: self)
        case 2:
            dayComponent.month = -1
            return Calendar.current.date(byAdding: dayComponent, to: self)
        case 3:
            dayComponent.month = -3
            return Calendar.current.date(byAdding: dayComponent, to: self)
        case 4:
            dayComponent.month = -6
            return Calendar.current.date(byAdding: dayComponent, to: self)
        case 5:
            dayComponent.year = -1
            return Calendar.current.date(byAdding: dayComponent, to: self)
        case 6:
            dayComponent.year = -2
            return Calendar.current.date(byAdding: dayComponent, to: self)
        case 7:
            dayComponent.year = -5
            return Calendar.current.date(byAdding: dayComponent, to: self)
        case 8:
            dayComponent.year = -10
            return Calendar.current.date(byAdding: dayComponent, to: self)
        default:
            return self
        }
    }

    
    //["1D","1W","1M","3M","6M","1Y","2Y","5Y","10Y"]
    
    /*
    init(fromServerString dateString: String?) {
       /*if dateString == nil {
            return nil
        }
        if dateString == (NSNull() as? String) {
            return NSNull() as? Date
        }*/
        let serverDateFormatter = Date.serverDateFormatter()
        _ = serverDateFormatter?.date(from: dateString ?? "")
        return self
    }*/


}

extension UITextField {
    func setLeftPaddingPoints(_ amount:CGFloat){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
    func setRightPaddingPoints(_ amount:CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.rightView = paddingView
        self.rightViewMode = .always
    }
}

extension UINavigationBar{
    
    func navigationStyle() {
        setBackgroundImage(UIImage(named: "gradient.png"), for: .default)
        barTintColor = UIColor.rwColorBlueButton()
        isTranslucent = false
        isHidden = false
        titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        // self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
        tintColor = UIColor.white
    }

}



