//
//  RWSequrityKeyboardVC.swift
//  Rocket Wallet Swift
//
//  Created by Иван Петрашко on 10.09.2018.
//  Copyright © 2018 Иван§ Петрашко. All rights reserved.
//

import UIKit
import RealmSwift

class RWSequrityKeyboardVC: UIViewController,UICollectionViewDataSource,UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 6
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! RWCellPasscodeDot
      //  cell.dot.backgro = UIColor.red
        if (self.textfield.text?.count)! > indexPath.row{
            cell.dot.backgroundColor = UIColor.darkGray
        } else {
             cell.dot.backgroundColor = UIColor.clear
        }
    
        cell.dot.layer.borderColor = UIColor.darkGray.cgColor
        cell.dot.layer.borderWidth = 2
        cell.dot.layer.cornerRadius = 10
        cell.dot.layer.masksToBounds = true
        return cell
    }
    

    @IBOutlet weak var textfield: UITextField!
    @IBOutlet weak var textlavel: UILabel!
    var povtor:Bool = false
    var change:Bool = false
    var first:String?
    @IBOutlet weak var textBad: UILabel!
    
    var sign:Bool = false
    var textFirst = ""
    var textTwo = ""
    
    @IBOutlet weak var collection: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.textfield.becomeFirstResponder()
        self.textfield.addTarget(self, action: #selector(self.keyboardChange(text:)), for: .editingChanged )
        // Do any additional setup after loading the view.
        self.textlavel.text = textFirst
        
        self.textBad.text = "Неудачная попытка"
        self.textBad.layer.cornerRadius = 10
        self.textBad.layer.masksToBounds = true
        self.textBad.backgroundColor = UIColor(red:0.98, green:0.27, blue:0, alpha:1)
        self.textBad.textColor = UIColor.white
        self.textBad.isHidden = true
        
        self.title = "Passcode"
        
        self.collection.delegate = self
        self.collection.dataSource = self
        
        navigationController?.navigationBar.navigationStyle()
        let cancel = UIBarButtonItem(title: "Cancel", style: .done, target: self, action: #selector(self.cancelFunc))
        navigationItem.leftBarButtonItem = cancel
    }
    
    
    
    @objc func cancelFunc() {
        navigationController?.dismiss(animated: true)
    }
    
    @objc func keyboardChange(text:UITextField){
        collection.reloadData()
        if text.text?.count == 6{
            if(sign){
                if RWWallet.getWallet().setting.getPasscodeString() == text.text{
                    self.dismiss(animated: true) {
                         NotificationCenter.default.post(name: NSNotification.Name("OpenSequrity"), object: nil)
                    }
                } else {
                    self.badLabel(str: "Неверный ввод")
                }
            } else {
            if povtor {
                if change || first == text.text {
                RWWallet.getWallet().setting.setPasscodeString(str: text.text!)
                let realm = try! Realm()
                   try! realm.beginWrite()
                    RWWallet.getWallet().setting.passcode = true
                   try! realm.commitWrite()
                self.dismiss(animated: true) {
                    
                }
                } else {
                    self.badLabel(str: "Коды не совпадают")
                }
            } else {
                if change {
                    if RWWallet.getWallet().setting.getPasscodeString() == text.text {
                         self.oldPassBad(text: text)
                    } else {
                         self.badLabel(str: "Код неверен")
                    }
                } else {
                   self.oldPassBad(text: text)
                }
              
            }
        }
        }
    }
    
    func oldPassBad(text:UITextField){
        first = text.text
        text.text = ""
        self.povtor = true
        self.textlavel.text = textTwo
        self.textBad.isHidden = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func badLabel(str:String){
        self.textBad.text = str
        self.textBad.isHidden = false
        self.textfield.text = ""
        self.povtor = false
        //if(sign){
            self.textlavel.text = textFirst
       // }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
