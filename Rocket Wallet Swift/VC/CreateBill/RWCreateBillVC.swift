//
//  RWCreateBillVC.swift
//  Rocket Wallet Swift
//
//  Created by Иван Петрашко on 13.08.2018.
//  Copyright © 2018 Иван§ Петрашко. All rights reserved.
//

import UIKit
import Web3
import QRCodeReader
import AVFoundation

class RWCreateBillVC: UIViewController, UITableViewDelegate, UITableViewDataSource,UIScrollViewDelegate, QRCodeReaderViewControllerDelegate {
    
    var token: RWToken!
    var numberToolbar:UIToolbar! = nil
    
    var bill:RWBill = RWBill()
    
    var labelInCurrency:UILabel?
    var taxFeelabelSpeed:UILabel?
    var fieldInToken:UITextField?
    var buttonFeeSum:UIButton?
    
    @IBOutlet weak var bottomConstrait: NSLayoutConstraint!
    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet weak var allSumCurrency: UILabel!
    @IBOutlet weak var allSumToken: UILabel!
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "CellHeader", for: indexPath) as! RWCellTokenHeader
            cell.viewLabel.isHidden = true;
            cell.eth.text = String(format: "%@ %@",(self.bill.token?.symbol)!,(self.bill.token?.getBalanceString())!)
            cell.sum.text = self.bill.token?.getInCurrencyString()
            cell.imageProfile.image = self.bill.token?.logo?.getImage(tableView, andNotification: nil)
            cell.sumchange.isHidden = true
            cell.percentchange.isHidden = true
            cell.currency.isHidden = true
            cell.h24.isHidden = true
            cell.arr.isHidden = true
            return cell;
        } else if indexPath.row == 1{
            let cell = tableView.dequeueReusableCell(withIdentifier: "RWCellCreateBillSendTo", for: indexPath) as! RWCellCreateBillSendTo
            cell.setStyle()
            cell.selectionStyle = .none
            cell.address.addTarget(self, action: #selector(self.addressChange), for: .editingChanged)
            cell.address.text = bill.address
            cell.scanButton.addTarget(self, action: #selector(self.scanInModalAction(_:)), for: .touchUpInside)
            return cell;
        } else if indexPath.row == 2{
            let cell = tableView.dequeueReusableCell(withIdentifier: "RWCellCreateBillAmount", for: indexPath) as!  RWCellCreateBillAmount
            cell.setStyle()
            cell.selectionStyle = .none
            if !bill.revers {
                cell.sumLabel.text = bill.token.symbol
                cell.perevodLabel.text = "USD"
                cell.changecurrency.setTitle("USD", for: .normal)
            } else {
                cell.sumLabel.text = "USD"
                cell.perevodLabel.text = bill.token.symbol
                cell.changecurrency.setTitle(bill.token.symbol, for: .normal)
            }
            
            cell.changecurrency.addTarget(self, action: #selector(self.changeCurrency), for: .touchUpInside)
            self.labelInCurrency = cell.perevodsum
            self.fieldInToken = cell.fieldSum
            cell.perevodsum.text = bill.getInCurrency()
            cell.fieldSum.addTarget(self, action: #selector(self.sumChange), for: .editingChanged)
            cell.fieldSum.text = bill.sum
            cell.useall.addTarget(self, action: #selector(self.useallFunc), for: .touchUpInside)
            return cell;
        } else if indexPath.row == 3{
            let cell = tableView.dequeueReusableCell(withIdentifier: "RWCellCreateBillTaxFee", for: indexPath) as!  RWCellCreateBillTaxFee
            cell.setStyle()
            cell.selectionStyle = .none
            cell.feesum.setTitle(bill.getTxFeeCurrency(), for: .normal)
            self.buttonFeeSum = cell.feesum
            cell.slider.minimumValue = 1000000000
            cell.slider.maximumValue = Float(bill.gasPrice)*3
            cell.slider.value = Float(bill.gasPriceOnline)
            self.taxFeelabelSpeed = cell.feeSpeed
            //print(Float(bill.gasPrice/1000000000)*3)
            cell.slider.addTarget(self, action: #selector(self.changeSlider(slider:)), for: .valueChanged)
            return cell;
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CellHeader", for: indexPath) as! RWCellTokenHeader
            return cell;
        }
    }
    
    @objc func changeSlider(slider:UISlider)->Void{
         bill.gasPriceOnline = BigUInt(slider.value)
        updateLabelsSum()
       
    }
    
    func updateLabelsSum()->Void{
        self.labelInCurrency?.text = bill.getInCurrency()
        self.buttonFeeSum?.setTitle(bill.getTxFeeCurrency(), for: .normal)
        self.allSumToken.text = bill.getAllSumAndTax()
        self.allSumCurrency.text = bill.getAllSumInCurrency()
        self.taxFeelabelSpeed?.text = bill.getTaxFeeSpeedLabel()
        self.taxFeelabelSpeed?.textColor = bill.getTaxFeeSpeedColor()
    }
    
    @objc func changeCurrency()->Void{
        bill.reversChange()
        self.table.reloadData()
    }
    
    @objc func useallFunc()->Void{
        bill.useAll()
        updateLabelsSum()
        self.table.reloadData()
    }
    
    @objc func addressChange(field:UITextField) -> Void {
        bill.address = field.text!
    }
    
    @objc func sumChange(field:UITextField) -> Void {
      //  if field.text?.range(of: ",") != nil{
            field.text = field.text?.replacingOccurrences(of: ",", with: ".")
       // }
        if !(field.text?.isValidDouble(maxDecimalPlaces: 20))!{
            field.text = String((field.text?.dropLast())!)
        }
        bill.sum = field.text!
        updateLabelsSum()
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if indexPath.row == 0 {
            let portfolio = UIStoryboard(name: "CreateBill", bundle: nil).instantiateViewController(withIdentifier: "RWChangePaymentVC") as! RWChangePaymentVC
           // portfolio.token = RWWallet.getWallet().getSortTokens()[indexPath.row]
            self.navigationController?.pushViewController(portfolio, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0{
            return 89
        } else if indexPath.row == 1{
            return 77
        } else if indexPath.row == 2{
            return 117
        } else if indexPath.row == 3{
            return 76
        } else {
            return 77
        }
    }
    

    @IBOutlet weak var table: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        table.register(UINib(nibName: "Cell", bundle: nil), forCellReuseIdentifier: "CellHeader")
        table.delegate = self
        table.dataSource = self
        
        self.bill.token = self.token
        self.title = "Create Bill"
        navigationController?.navigationBar.navigationStyle()
        let cancel = UIBarButtonItem(title: "Cancel", style: .done, target: self, action: #selector(self.cancelFunc))
        navigationItem.leftBarButtonItem = cancel
        
        self.allSumToken.font = UIFont.setFontStyle(style: .SemiLight, size: 17)
        self.allSumCurrency.font = UIFont.setFontStyle(style: .SemiLight, size: 12)
        
        self.allSumCurrency.textColor = UIColor.rwColorGrayGraph()
        self.allSumCurrency.textColor = UIColor.darkGray
        
        self.sendButton.setButtonStyle(style: .SendBill)
        self.sendButton.setTitle("Send", for: .normal)
        
        self.sendButton.addTarget(self, action: #selector(self.sendButtFunc), for: .touchUpInside)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.setPayment), name: NSNotification.Name(rawValue: "setPaymentToken"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.sliderUpdate), name: NSNotification.Name(rawValue: "sliderUpdate"), object: nil)
        bill.getGasPrice()
        updateLabelsSum()
    }
    
    @objc func sliderUpdate(){
        self.updateLabelsSum()
        self.table.reloadData()
    }
    
    @objc func sendButtFunc(){
        let portfolio = UIStoryboard(name: "CreateBill", bundle: nil).instantiateViewController(withIdentifier: "RWSendBillVC") as! RWSendBillVC
        portfolio.modalPresentationStyle = .overCurrentContext
        portfolio.bill = self.bill
        present(portfolio, animated: true)
    }
    
    
    
    @objc func setPayment(not:Notification) -> Void {
        self.bill.token = not.object as! RWToken
        bill.getGasPrice()
        bill.sum = ""
        self.table.reloadData()
    }

    @objc func keyboardWillShow(_ notification: Notification) {
        if let keyboardFrame: NSValue = notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            let keyboardHeight = keyboardRectangle.height
            self.bottomConstrait.constant = 0 - keyboardHeight
           // self.layo
            self.view.layoutIfNeeded()
        }
    }

    @objc func keyboardWillHide(notification: NSNotification){
        self.bottomConstrait.constant = 0;
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
         view.endEditing(true)
    }
    
    @objc func cancelFunc() {
        navigationController?.dismiss(animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //QR SCANER
    
    lazy var reader: QRCodeReader = QRCodeReader()
    lazy var readerVC: QRCodeReaderViewController = {
        let builder = QRCodeReaderViewControllerBuilder {
            $0.reader                  = QRCodeReader(metadataObjectTypes: [.qr], captureDevicePosition: .back)
            $0.showTorchButton         = true
            $0.preferredStatusBarStyle = .lightContent
            
            $0.reader.stopScanningWhenCodeIsFound = false
        }
        
        return QRCodeReaderViewController(builder: builder)
    }()
    
    // MARK: - Actions
    
    private func checkScanPermissions() -> Bool {
        do {
            return try QRCodeReader.supportsMetadataObjectTypes()
        } catch let error as NSError {
            let alert: UIAlertController
            
            switch error.code {
            case -11852:
                alert = UIAlertController(title: "Error", message: "This app is not authorized to use Back Camera.", preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: "Setting", style: .default, handler: { (_) in
                    DispatchQueue.main.async {
                        if let settingsURL = URL(string: UIApplicationOpenSettingsURLString) {
                            UIApplication.shared.openURL(settingsURL)
                        }
                    }
                }))
                
                alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            default:
                alert = UIAlertController(title: "Error", message: "Reader not supported by the current device", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
            }
            
            present(alert, animated: true, completion: nil)
            
            return false
        }
    }
    
    @IBAction func sendButt(_ sender: Any) {
        bill.validation(block: {
            self.sendButtFunc()
        }, blockError: { string in
            let alert = UIAlertController(title: "Error", message: string, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
        })
    }
    
    @objc func scanInModalAction(_ sender: AnyObject) {
        guard checkScanPermissions() else { return }
        
        readerVC.modalPresentationStyle = .formSheet
        readerVC.delegate               = self
        
        readerVC.completionBlock = { (result: QRCodeReaderResult?) in
            if let result = result {
                print("Completion with result: \(result.value) of type \(result.metadataType)")
            }
        }
        
        present(readerVC, animated: true, completion: nil)
    }
    
    func reader(_ reader: QRCodeReaderViewController, didScanResult result: QRCodeReaderResult) {
        reader.stopScanning()
        
        dismiss(animated: true) { [weak self] in
            /*
            let alert = UIAlertController(
                title: "QRCodeReader",
                message: String (format:"%@ (of type %@)", result.value, result.metadataType),
                preferredStyle: .alert
            )
            alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
            
            self?.present(alert, animated: true, completion: nil)*/
            self?.bill.address = result.value
            self?.table.reloadData()
        }
    }
    
    func reader(_ reader: QRCodeReaderViewController, didSwitchCamera newCaptureDevice: AVCaptureDeviceInput) {
        print("Switching capturing to: \(newCaptureDevice.device.localizedName)")
    }
    
    func readerDidCancel(_ reader: QRCodeReaderViewController) {
        reader.stopScanning()
        
        dismiss(animated: true, completion: nil)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
