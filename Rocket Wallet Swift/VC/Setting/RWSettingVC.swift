
//
//  RWSettingVC.swift
//  Rocket Wallet Swift
//
//  Created by Иван Петрашко on 23.08.2018.
//  Copyright © 2018 Иван§ Петрашко. All rights reserved.
//

import UIKit
import KeychainSwift
import RealmSwift

class RWSettingVC: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    var indexOpen = 0
    
    @objc func openSequrity(){
        if indexOpen == 0 {
            let portfolio = UIStoryboard(name: "Setting", bundle: nil).instantiateViewController(withIdentifier: "RWSettingSequrityVC") as! RWSettingSequrityVC
            self.navigationController?.pushViewController(portfolio, animated: true)
        } else {
            let portfolio = UIStoryboard(name: "Setting", bundle: nil).instantiateViewController(withIdentifier: "RWExportVC") as! RWExportVC
            self.navigationController?.pushViewController(portfolio, animated: true)
        }
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        } else if section == 0 {
            return 1
        } else {
            return 1
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
            let view = UIView.init(frame: CGRect.init(x: 0, y: 0, width: self.view.frame.width, height: 30))
            view.backgroundColor = UIColor.rwColorGrayBackground()
            let label = UILabel.init(frame: CGRect.init(x: 16, y: 0, width: 200, height: 34))
            label.font = UIFont.setFontStyle(style: .SemiLight, size: 13)
            label.textColor = UIColor.darkGray
            if(section == 0){
                label.text = "Sequrity"
            } else if section == 1 {
                label.text = "Export"
            } else if section == 2 {
                label.text = ""
        }
            view.addSubview(label)
            return view
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        if indexPath.section == 0 {
            cell.textLabel?.text = "Touch ID and Passcode"
        } else if indexPath.section == 1 {
            cell.textLabel?.text = "Export"
        } else if indexPath.section == 2 {
            cell.textLabel?.text = "Log out"
        }
        cell.accessoryType = .disclosureIndicator
        cell.textLabel?.font = UIFont.setFontStyle(style: .SemiLight, size: 17)
        cell.textLabel?.textColor = UIColor.darkGray
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        indexOpen = indexPath.section
         if indexPath.section == 0 || indexPath.section == 1{
            if !RWWallet.getWallet().setting.passcode {
                self.openSequrity()
            } else {
                let portfolio = UIStoryboard(name: "Sequrity", bundle: nil).instantiateViewController(withIdentifier: "RWSequrityKeyboardVC") as! RWSequrityKeyboardVC
                portfolio.sign = true
                portfolio.textFirst = "Введите код пароль"
                var navigationFirst: UINavigationController! = nil
                navigationFirst = UINavigationController(rootViewController: portfolio)
                present(navigationFirst, animated: true)
            }
        } else if indexPath.section == 2 {
            let alert = UIAlertController.init(title: "Log Out", message: "Please make sure your wallet are properly backed up.", preferredStyle: .alert)
            alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
            alert.addAction(UIAlertAction.init(title: "Log Out", style: .default, handler: { (action) in
                self.logOut()
            }))
            self.present(alert, animated: true, completion: nil)
         }
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func logOut(){
        let keychain = KeychainSwift()
        keychain.set("", forKey: "privateKey")
        let realm = try! Realm()
        try! realm.write {
            realm.deleteAll()
        }
        
        if (UIApplication.shared.keyWindow?.rootViewController?.isKind(of: RWTabBar.self))!{
            let portfolio = UIStoryboard(name: "SignIn", bundle: nil).instantiateViewController(withIdentifier: "SignInViewController") as! SignInViewController
            UIApplication.shared.keyWindow?.rootViewController = portfolio
        } else {
            self.tabBarController?.dismiss(animated: true, completion: nil)
        }
    }
    
   

    
    @IBOutlet weak var table: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Setting"
        navigationController?.navigationBar.navigationStyle()
        self.table.delegate = self
        self.table.dataSource = self
        NotificationCenter.default.addObserver(self, selector: #selector(self.openSequrity), name: NSNotification.Name("OpenSequrity"), object: nil)
        self.view.backgroundColor = UIColor.rwColorGrayBackground()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
