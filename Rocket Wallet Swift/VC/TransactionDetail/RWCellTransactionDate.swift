//
//  RWCellTransactionDate.swift
//  Rocket Wallet Swift
//
//  Created by Иван Петрашко on 04.09.2018.
//  Copyright © 2018 Иван§ Петрашко. All rights reserved.
//

import UIKit

class RWCellTransactionDate: UITableViewCell {

    @IBOutlet weak var title1: UILabel!
    @IBOutlet weak var title2: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.title1.font = UIFont.setFontStyle(style: .SemiLight, size: 16)
        self.title1.textColor = UIColor.darkGray
        
        self.title2.font = UIFont.setFontStyle(style: .SemiLight, size: 18)
        self.title2.textColor = UIColor.darkGray
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

class RWCellTransactionSum: UITableViewCell {
    
    @IBOutlet weak var title1: UILabel!
    @IBOutlet weak var title2: UILabel!
    @IBOutlet weak var title3: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.title1.font = UIFont.setFontStyle(style: .SemiLight, size: 16)
        self.title1.textColor = UIColor.darkGray
        
        self.title2.font = UIFont.setFontStyle(style: .SemiLight, size: 18)
        self.title2.textColor = UIColor.darkGray
        
        self.title3.font = UIFont.setFontStyle(style: .SemiLight, size: 16)
        self.title3.textColor = UIColor.darkGray
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}

class RWCellSettingSwitch: UITableViewCell {
    
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var switchbutton: UISwitch!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.title.font = UIFont.setFontStyle(style: .SemiLight, size: 16)
        self.title.textColor = UIColor.darkGray
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}



