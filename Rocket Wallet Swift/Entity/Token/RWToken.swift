//
//  RWToken.swift
//  Rocket Wallet Swift
//
//  Created by Иван Петрашко on 20.07.2018.
//  Copyright © 2018 Иван§ Петрашко. All rights reserved.
//

import UIKit
import Realm
import RealmSwift
import Web3

class RWToken: Object {
    @objc dynamic var symbol = ""
    @objc dynamic var address = ""
    @objc dynamic var decimals: Int = 0
    @objc dynamic var sortNumber: Int = 0
    @objc dynamic var name = ""
    @objc dynamic var balance:NSNumber = 0.0
    @objc dynamic var change24h:NSNumber = 0.0
    @objc dynamic var logo: RWImage?
    var transactions:List<RWTransaction> = List.init()
    
    var balanceList:List<RWBalance> = List.init()
    
    class func getMyToken(withPublicKey key: String?, andBlock block: @escaping (_ tokens: [RWToken]?) -> Void, andError blockError: @escaping (_ string: String?) -> Void) {
        API.sharedInstance.request(url: String(format: "tokens/%@", key!), completion: { (dict:Any) in
            var arrTrans: [RWToken] = []
            let arr = dict as? [NSDictionary]
            if arr != nil {
            for dict in arr!{
                let token = RWToken()
                token.address = dict["address"] as! String
                token.decimals = dict["decimals"] as! Int
                token.name = dict["name"] as! String
                token.symbol = (dict["symbol"] as! String).uppercased()
                token.balance = dict["balance"] as! NSNumber
                token.sortNumber = arrTrans.count 
                token.initImage()
                arrTrans.append(token)
            }
            }
            block(arrTrans)
        }) { (str:String) in
            blockError(str);
        }
    }
    
    func getChangeUSD()->Double{
        if self.balanceList.count>0{
            return self.balanceList.last!.totalUSD - self.balanceList.first!.totalUSD
        }
         return 0.0
    }
    
    func getChangePercent()->Double{
        if self.balanceList.count>0{
            if getChangeUSD() == 0 && self.balanceList.last!.totalUSD == 0 {
                 return 0.0
            } else if self.balanceList.first!.totalUSD == 0 && self.balanceList.last!.totalUSD>0 {
                return 100.0
            } else if self.balanceList.first!.totalUSD > 0 && self.balanceList.last!.totalUSD == 0{
                return -100.0
            } else {
                return  getChangeUSD() * 100 / self.balanceList.last!.totalUSD
            }
           // print(String(format: "percent = %.2f      %.2f",  getChangeUSD(),self.balanceList.last!.totalUSD))
            
        }
        return 0.0
    }
    
    func getBalanceList(dateFrom:Date,dateTo:Date,block: @escaping (_ balance: List<RWBalance>) -> Void, andError blockError: @escaping (_ string: String?) -> Void){
        let listP = List<RWToken>.init()
        listP.append(self)
        RWBalance.getBalanceList(tokens: listP, dateFrom: dateFrom, dateTo: dateTo, points:30 , block: { (list) in
            self.balanceList = list
            block(list)
        }) { (str) in
             blockError(str);
        }
    }
    
    func updateBalanceFunc(bal:NSNumber){
        do{
        let realm = try! Realm()
        // realm.beginWrite()
        try realm.write {
            self.balance = bal
            // realm.commitWrite()
        }
        } catch {
            
        }
    }
    
    func getTransaction(block: @escaping (_ complete:Bool) -> Void,blockError: @escaping (_ string: String?) -> Void){
        let listOne:List<RWToken> = List.init()
        listOne.append(self)
        RWTransaction.getTransaction(tokens: listOne, fromDate: Date(timeIntervalSinceReferenceDate: -123456789.0), todate: Date(), block: { (transaction) in
            self.transactions = transaction
            block(true)
        }) { (error) in
           // print(error)
            blockError(error)
        }
    }
    
    func updateBalabce()->Void{
        if self.name == "ETH"{
            do {
                let web3 = Web3(rpcURL: Constants.URL_NODA)
                let address = try EthereumAddress(hex: RWWallet.getWallet().publicKey, eip55: false)
                web3.eth.getBalance(address: address, block: .latest) { response in
                    if response.status.isSuccess == true {
                          DispatchQueue.main.async {
                            self.updateBalanceFunc(bal: NSNumber(floatLiteral: Double((response.result?.quantity)!) / pow(10,Double(self.decimals))))
                         }
                    }
                }
            } catch {
                
            }
        } else {
         do {
            let web3 = Web3(rpcURL: Constants.URL_NODA)
            let contractAddress = try EthereumAddress(hex: self.address, eip55: false)
            let contract = web3.eth.Contract(type: GenericERC20Contract.self, address: contractAddress)
        firstly {
            try contract.balanceOf(address: EthereumAddress(hex: RWWallet.getWallet().publicKey, eip55: false)).call()
            }.done { outputs in
                var bigbalance = outputs["_balance"] as? BigUInt
                if bigbalance == nil{
                    bigbalance = 0
                }
                let realm = try! Realm()
                try realm.write {
                    print("update balance")
                   self.balance = NSNumber(floatLiteral: Double(bigbalance!) / pow(10,Double(self.decimals)))
                }
                
               //self.balance = NSNumber(floatLiteral: (outputs["_balance"] as? BigUInt) / self.decimals as BigUInt)
                //print(outputs["_balance"] as? BigUInt)
            }.catch { error in
                print(error)
            }
         } catch {
            
        }
    }
    }
    
    func getInCurrencyString() -> String? {
        let sign = getInCurrencyDouble(true)
        if sign != 0 {
            return String(format: "$ %.2f", sign)
        } else {
            return "$ 0.00"
        }

    }
    
    func getInCurrencyDouble(_ online: Bool) -> Double {
       // var rateOnline = RWRate.objects(with: NSPredicate(format: "tokenSymbol == %@ && online == %@", symbol, online)).first as? RWRate
        let realm = try! Realm()
        let rateOnline = realm.objects(RWRate.self).filter(NSPredicate(format: "tokenSymbol == %@ && online == %@", self.symbol, NSNumber(value: online))).first
        
        
        if rateOnline != nil {
            return balance.doubleValue * rateOnline!.rate.doubleValue
        } else {
            return 0
        }
    }
    
    func getRateInString() -> String? {
        let realm = try! Realm()
        
        
        print("новая строка")
        print(self.symbol)
         print(realm.objects(RWRate.self))
        
        let rateOnline = realm.objects(RWRate.self).filter(NSPredicate(format: "tokenSymbol == %@ && online == %@", self.symbol, NSNumber(value: true))).first
        if rateOnline != nil {
            return "\(String(format: "$ %.2f", (rateOnline?.rate.doubleValue)!))"
        } else {
            return "0"
        }
    }
    
    func getRate() -> Double? {
        let realm = try! Realm()
        let rateOnline = realm.objects(RWRate.self).filter(NSPredicate(format: "tokenSymbol == %@ && online == %@", self.symbol, NSNumber(value: true))).first
        if rateOnline != nil {
            return rateOnline?.rate.doubleValue
        } else {
            return 0.0
        }
    }
    
    func getChangeRate() -> String? {
        let realm = try! Realm()
        let rateOnline = realm.objects(RWRate.self).filter(NSPredicate(format: "tokenSymbol == %@ && online == %@", self.symbol, NSNumber(value: true))).first
        let rateOld = realm.objects(RWRate.self).filter(NSPredicate(format: "tokenSymbol == %@ && online == %@", self.symbol, NSNumber(value: false))).first
        if (rateOnline != nil) && (rateOld != nil) {
        let changerate: Double = rateOld!.rate.doubleValue - rateOnline!.rate.doubleValue
        var minus: Double = 100 * changerate / rateOnline!.rate.doubleValue
        if minus < 0 {
            minus = minus + (minus * -2)
        }
            return String(format: "%.0f%@",minus,"%")
        } else {
            return ""
        }

    }
    
    class func getEthRate()->Double{
        let realm = try! Realm()
        let rateOnline = realm.objects(RWRate.self).filter(NSPredicate(format: "tokenSymbol == %@ && online == %@", "ETH", NSNumber(value: true))).first
        if rateOnline != nil {
            return rateOnline!.rate.doubleValue
        } else {
            return 0.0
        }
    }
    
    func getChangeRatePositiv() -> Bool {
        let realm = try! Realm()
        let rateOnline = realm.objects(RWRate.self).filter(NSPredicate(format: "tokenSymbol == %@ && online == %@", self.symbol, NSNumber(value: true))).first
        let rateOld = realm.objects(RWRate.self).filter(NSPredicate(format: "tokenSymbol == %@ && online == %@", self.symbol, NSNumber(value: false))).first
        if (rateOnline != nil) && (rateOld != nil) {
        let changerate: Double = rateOld!.rate.doubleValue - rateOnline!.rate.doubleValue
        if changerate < 0 {
            return true
        } else {
            return false
            } } else {
        return true
        }
    }
    
    func getBalanceString() -> String? {
        if (self.balance != nil) {
            return "\(String(format: "%f", self.balance.doubleValue))"
        } else {
            return "0.00"
        }
    }
    
    func initImage() {
        
        
        if let path = Bundle.main.path(forResource: "tokens-eth", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves)
                //print(jsonResult);
                //jsonResult
               // print("re")
                let arrFilter:Array = (jsonResult as! NSArray).filtered(using: NSPredicate(format: "symbol == %@", symbol))
              //  let arrFilter = (jsonResult as Array)
                var urls = ""
                
                let arr = arrFilter as? [NSDictionary]
                for dict in arr!{
                    let dict2 = dict["logo"] as! NSDictionary
                    urls = dict2["src"] as! String
                    logo = RWImage()
                    logo?.url = urls
                    print(urls)
                }
                    
                    
             //   if arrFilter.count > 0 {
                   // let dict = arrFilter.
                    
                  //  urls = arrFilter[0]["logo"]["src"] as String
                  //  urls = arrFilter[0]["logo"]!["src"] as String
             //   }
               // logo = RWImage()
               // logo?.url = urls
              /*  if let jsonResult = jsonResult as? Dictionary<String, AnyObject>, let person = jsonResult["person"] as? [Any] {
                    // do stuff
                }*/
            } catch {
                // handle error
            }
        }
        
        /*
        var filePath = ""
        filePath = Bundle.main.path(forResource: "tokens-eth", ofType: "json") ?? ""
        var myJSON = try? String(contentsOfFile: filePath, encoding: .utf8)
        var error: Error? = nil
        var jsonDataArray = []
        if let anEncoding = myJSON?.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue)) {
            jsonDataArray = try! JSONSerialization.jsonObject(with: anEncoding, options: []) as? [Any]
        }
        var arrFilter:NSArray = (jsonDataArray as NSArray?)?.filtered(using: NSPredicate(format: "symbol == %@", symbol))
        var url = ""
        if arrFilter?.count == 1 {
            url = arrFilter![0]["logo"]["src"] as? String ?? ""
        }
        logo = RWImage()
        logo?.url = url
        print("url logo = \(logo?.url)")
         */
    }

}
