//
//  RWSendBillVC.swift
//  Rocket Wallet Swift
//
//  Created by Иван Петрашко on 29.08.2018.
//  Copyright © 2018 Иван§ Петрашко. All rights reserved.
//
import UIKit
import SmileLock
import LocalAuthentication

class RWSendBillVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var containsTop: NSLayoutConstraint!
    var bill:RWBill!
    var passwordUIValidation: MyPasswordUIValidation!
    var heightTouch:CGFloat = 80
    var sendActionProcess:Bool = false
    var heightAll:CGFloat = 300
    //let passwordContainerView: PasswordContainerView = ...
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
           let cell = tableView.dequeueReusableCell(withIdentifier: "RWCellBillLabel", for: indexPath) as! RWCellBillLabel
           cell.label.font = UIFont.setFontStyle(style: .SemiLight, size: 17)
           cell.backgroundColor = UIColor.clear
           cell.selectionStyle = .none
           return cell
            //break
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "RWCellBillAdress", for: indexPath) as! RWCellBillAdress
            cell.label.font = UIFont.setFontStyle(style: .SemiLight, size: 15)
            cell.address.font = UIFont.setFontStyle(style: .SemiLight, size: 15)
            cell.address.text = bill.address
            cell.backgroundColor = UIColor.clear
            cell.selectionStyle = .none
            return cell
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: "RWCellBillAmount", for: indexPath) as! RWCellBillAmount
            cell.label.font = UIFont.setFontStyle(style: .SemiLight, size: 15)
            cell.count.font = UIFont.setFontStyle(style: .SemiLight, size: 15)
            cell.currency.font = UIFont.setFontStyle(style: .SemiLight, size: 15)
            cell.backgroundColor = UIColor.clear
            
            cell.label.text = "Amount"
            cell.count.text = bill.getAllSum()
            cell.currency.text = bill.getAmountInCurrenctBill()
            cell.selectionStyle = .none
            return cell
        case 3:
            let cell = tableView.dequeueReusableCell(withIdentifier: "RWCellBillAmount", for: indexPath) as! RWCellBillAmount
            cell.label.font = UIFont.setFontStyle(style: .SemiLight, size: 15)
            cell.count.font = UIFont.setFontStyle(style: .SemiLight, size: 15)
            cell.currency.font = UIFont.setFontStyle(style: .SemiLight, size: 15)
            
            cell.label.text = "TxFee"
            cell.count.text = bill.getTxFeeETHString()
            cell.currency.text = bill.getTxFeeCurrency()
            cell.selectionStyle = .none
            cell.backgroundColor = UIColor.clear
            return cell
        case 4:
            let cell = tableView.dequeueReusableCell(withIdentifier: "RWCellBillAmount", for: indexPath) as! RWCellBillAmount
            cell.label.font = UIFont.setFontStyle(style: .SemiLight, size: 15)
            cell.count.font = UIFont.setFontStyle(style: .SemiLight, size: 15)
            cell.currency.font = UIFont.setFontStyle(style: .SemiLight, size: 15)
            
            cell.label.text = "Total"
            cell.count.text = bill.getAllSumAndTaxForBill()
            cell.currency.text = bill.getAllSumInCurrency()
            cell.backgroundColor = UIColor.clear
            cell.separatorInset = UIEdgeInsets(top: 0, left: tableView.frame.size.width, bottom: 0, right: 0)
            cell.selectionStyle = .none
            return cell

        case 5:
            if RWWallet.getWallet().setting.getPasscodeEnabled() {
            let cell = tableView.dequeueReusableCell(withIdentifier: "RWCellBillSendPasscode", for: indexPath) as! RWCellBillSendPasscode
            cell.layer.masksToBounds = true
            cell.selectionStyle = .none
            cell.separatorInset = UIEdgeInsets(top: 0, left: tableView.frame.size.width, bottom: 0, right: 0)
            if passwordUIValidation == nil {
            passwordUIValidation = MyPasswordUIValidation(in: cell.stack)
            
            passwordUIValidation.success = { [weak self] _ in
                print("*️⃣ success!")
               // self?.bill.sendBill()
                
                 self?.sendBillAction()
                //self?.dismiss(animated: true, completion: nil)
            }
            
            passwordUIValidation.failure = {
                //do not forget add [weak self] if the view controller become nil at some point during its lifetime
                print("*️⃣ failure!")
            }
            
            //visual effect password UI
            //passwordUIValidation.view.rearrangeForVisualEffectView(in: self)
            
            passwordUIValidation.view.deleteButtonLocalizedTitle = "Delete"
            passwordUIValidation.view.tintColor = UIColor.darkGray
            passwordUIValidation.view.highlightedColor = UIColor(red:0.75, green:0.75, blue:0.77, alpha:1)
            passwordUIValidation.view.touchAuthenticationEnabled = false
            if heightTouch == 80 {
                heightTouch = passwordUIValidation.view.frame.height
                self.updateContain(forpasscode: true)
                self.table.reloadData()
            }
            //passwordUIValidation.passwordInputComplete(passwordUIValidation.view, input: "1234")
            
         //   passwordUIValidation.validation =
            }
            
         //   cell.stack.tintColor = UIColor.red
            //cell.stack.highlightedColor = UIColor.blue
            
            cell.backgroundColor = UIColor.clear
                
                if sendActionProcess {
                    cell.indicator.isHidden = false
                    cell.stack.isHidden = true
                    cell.indicator.startAnimating()
                } else {
                    cell.indicator.isHidden = true
                    cell.stack.isHidden = false
                }
            return cell
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "RWCellBillSendButton", for: indexPath) as! RWCellBillSendButton
                cell.send.addTarget(self, action: #selector(self.sendBill), for: .touchUpInside)
                cell.backgroundColor = UIColor.clear
                cell.send.backgroundColor = UIColor.rwColorBlueButton()
                cell.send.layer.masksToBounds = true
                cell.send.layer.cornerRadius = 8
                cell.send.titleLabel?.font = UIFont.setFontStyle(style: .SemiLight, size: 17)
                cell.send.setTitleColor(UIColor.white, for: .normal)
                cell.separatorInset = UIEdgeInsets(top: 0, left: tableView.frame.size.width, bottom: 0, right: 0)
                cell.selectionStyle = .none
                if sendActionProcess {
                    cell.indicator.isHidden = false
                   // cell.indicator.
                    cell.indicator.startAnimating()
                    cell.send.isHidden = true
                } else {
                    cell.indicator.isHidden = true
                    cell.send.isHidden = false
                }
                return cell
            }
        default:
            return UITableViewCell.init()
        }
    }
    
    @objc func sendBill(){
        if RWWallet.getWallet().setting.touchOrFace {
           self.touchIdAction()
        } else {
           self.sendBillAction()
        }
    }
    
    func sendBillAction() -> Void {
        sendActionProcess = true
        self.table.reloadData()
        bill.sendBill(block: { (send) in
            self.sendActionProcess = false
            self.table.reloadData()
            self.dissmis()
          //  self.d
        }) { (alert) in
            self.sendActionProcess = false
            self.table.reloadData()
            let alert = UIAlertController(title: "Error", message: alert, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func touchIdAction() {
        
        print("hello there!.. You have clicked the touch ID")
        
        let myContext = LAContext()
        let myLocalizedReasonString = "Подтвердите действие"
        
        var authError: NSError?
        if #available(iOS 8.0, macOS 10.12.1, *) {
            if myContext.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &authError) {
                myContext.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: myLocalizedReasonString) { success, evaluateError in
                    
                    DispatchQueue.main.async {
                        if success {
                             self.sendBillAction()
                        } else {
                           // switchOn.setOn(false, animated: true)
                            self.alert(title: "Error", message: "User did not authenticate successfully")
                            // User did not authenticate successfully, look at error and take appropriate action
                            //  self.successLabel.text = "Sorry!!... User did not authenticate successfully"
                        }
                    }
                }
            } else {
               // switchOn.setOn(false, animated: true)
                self.alert(title: "Error", message: "Could not evaluate policy")
                // Could not evaluate policy; look at authError and present an appropriate message to user
                // successLabel.text = "Sorry!!.. Could not evaluate policy."
            }
        } else {
            // Fallback on earlier versions
           // switchOn.setOn(false, animated: true)
            self.alert(title: "Error", message: "This feature is not supported.")
            //   successLabel.text = "Ooops!!.. This feature is not supported."
        }
    }
    
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 0:
            return 40
        case 1:
            return 33
        case 2:
            return 55
        case 3:
            return 55
        case 4:
            return 55
        case 5:
            return heightTouch
        default:
            return 80
        }
    }
    

    @IBOutlet weak var buttBackground: UIButton!
    @IBOutlet weak var viewCorners: UIView!
    @IBOutlet weak var buttonDismis: UIButton!
    @IBOutlet weak var table: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.buttonDismis.addTarget(self, action: #selector(self.dissmis), for: .touchUpInside)
        self.buttBackground.addTarget(self, action: #selector(self.dissmis), for: .touchUpInside)
        // Do any additional setup after loading the view.
        self.buttBackground.alpha = 0
        
        self.viewCorners.layer.masksToBounds = true
        self.viewCorners.layer.cornerRadius = 20
        
        self.table.delegate = self
        self.table.dataSource = self
        
        self.table.backgroundColor = UIColor.clear
        self.updateContain(forpasscode: false)
    }
    
    func updateContain(forpasscode:Bool){
        
        let window = UIApplication.shared.keyWindow
        let topPadding = window?.safeAreaInsets.top
        let bottomPadding = window?.safeAreaInsets.bottom
        
        var heightScreen = UIScreen.main.bounds.height - bottomPadding!
        
        if !RWWallet.getWallet().setting.getPasscodeEnabled() {
            self.containsTop.constant = heightScreen - (heightAll + heightTouch)
        } else {
            if forpasscode {
                print(heightTouch)
                var screen =  heightScreen - (heightAll + heightTouch)
                if screen < 0{
                    screen = 0}
                    
                
                self.containsTop.constant = screen
            } else {
                self.containsTop.constant = 0
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        UIView.animate(withDuration: 0.2) {
            self.buttBackground.alpha = 0.5
        }
    }
    
    @objc func dissmis(){
        UIView.animate(withDuration: 0.1) {
            self.buttBackground.alpha = 0
        }
        self.dismiss(animated: true, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
