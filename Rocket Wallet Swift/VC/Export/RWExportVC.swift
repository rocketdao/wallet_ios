//
//  RWExportVC.swift
//  Rocket Wallet Swift
//
//  Created by Иван Петрашко on 06.09.2018.
//  Copyright © 2018 Иван§ Петрашко. All rights reserved.
//

import UIKit
import Keystore
import Web3
import EthereumKit


class RWExportVC: UIViewController {

    @IBOutlet weak var viewPhaseAndPrivate: UIView!
    @IBOutlet weak var exportKeystoneBtn: UIButton!
    @IBOutlet weak var pass1: UITextField!
    @IBOutlet weak var pass2: UITextField!
    @IBOutlet weak var exportKeystone: UILabel!
    @IBOutlet weak var descPhase: UILabel!
    
    @IBOutlet weak var copyBtn: UIButton!
    @IBOutlet weak var labelPhase: UILabel!
    @IBOutlet weak var imageQR: UIImageView!
    @IBOutlet weak var viewQR: UIView!
    @IBOutlet weak var viewKeystone: UIView!
    @IBOutlet weak var segment: UISegmentedControl!
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Export Wallet"
        // Do any additional setup after loading the view.
        self.segment.tintColor = UIColor.rwColorBlueButton()
        self.segment.addTarget(self, action: #selector(self.changeSegment(segment:)), for: .valueChanged)
        
        self.exportKeystone.font = UIFont.setFontStyle(style: .SemiLight, size: 13)
        self.exportKeystone.textColor = UIColor(red:0.54, green:0.54, blue:0.56, alpha:1)
        
        self.pass1.font = UIFont.setFontStyle(style: .SemiLight, size: 17)
        self.pass2.font = UIFont.setFontStyle(style: .SemiLight, size: 17)
        
        let image = generateQRCode(from: RWWallet.getWallet().getPrivateKey())
        self.imageQR.image = image
       // let addToken = UIBarButtonItem(title: "Import", style: .done, target: self, action: #selector(self.exportWallet))
       // navigationItem.rightBarButtonItem = addToken
        self.segmentStap(stap: ImportSegmentStap(rawValue: 0)!)
        
        self.exportKeystoneBtn.addTarget(self, action: #selector(self.exportKeystoneFunc), for: .touchUpInside)
        self.exportKeystoneBtn.setTitleColor(UIColor.rwColorBlueButton(), for: .normal)
        
        self.descPhase.font = UIFont.setFontStyle(style: .SemiLight, size: 16)
        self.labelPhase.font = UIFont.setFontStyle(style: .SemiLight, size: 16)
        
        self.copyBtn.addTarget(self, action: #selector(self.exportPrivateAndPhase), for: .touchUpInside)
        self.copyBtn.setTitleColor(UIColor.rwColorBlueButton(), for: .normal)
    }
    
    @objc func exportPrivateAndPhase(){
       // if ImportSegmentStap(rawValue: self.segment.selectedSegmentIndex)! == .Phase {
            UIPasteboard.general.string = self.labelPhase.text
     //   } else if ImportSegmentStap(rawValue: self.segment.selectedSegmentIndex)! == .Private{
            UIPasteboard.general.string = self.labelPhase.text
       // }
    }
    
    @objc func exportKeystoneFunc(){
        if (self.pass1.text?.count)!>0 &&  self.pass1.text == self.pass2.text {
        do{
            let privateKey = try EthereumPrivateKey(hexPrivateKey: "7743b7ddc437246c4c3cf81ae0d2b1299a4011fa7f67aec40bd9841d1d12544b")
            let keystore = try Keystore.init(privateKey: privateKey.makeBytes(), password: self.pass1.text!)
            let keystoreJson = try JSONEncoder().encode(keystore)
            
            let str = String(data: keystoreJson, encoding: String.Encoding.utf8) as! String
            let filename = getDocumentsDirectory().appendingPathComponent("keystone.json")
            
            do {
                try str.write(to: filename, atomically: true, encoding: String.Encoding.utf8)
                let objectsToShare = [filename]
                let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
                
                self.present(activityVC, animated: true, completion: nil)
            } catch {
                // failed to write file – bad permissions, bad filename, missing permissions, or more likely it can't be converted to the encoding
            }
        } catch {
            print("ok")
        }
        } else {
            let alert = UIAlertController.init(title: "Error", message: "Ошибка", preferredStyle: .alert)
            alert.addAction(UIAlertAction.init(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }
    
    func generateQRCode(from string: String) -> UIImage? {
        let data = string.data(using: String.Encoding.ascii)
        
        if let filter = CIFilter(name: "CIQRCodeGenerator") {
            filter.setValue(data, forKey: "inputMessage")
            let transform = CGAffineTransform(scaleX: 3, y: 3)
            
            if let output = filter.outputImage?.transformed(by: transform) {
                return UIImage(ciImage: output)
            }
        }
        
        return nil
    }
    
    @objc func changeSegment(segment:UISegmentedControl)->Void{
        self.segmentStap(stap: ImportSegmentStap(rawValue: segment.selectedSegmentIndex)!)
    }
    
    func segmentStap(stap:ImportSegmentStap)->Void{
        self.view.endEditing(true)
        switch stap {
        case .Phase:
            self.viewKeystone.isHidden = true
            self.viewQR.isHidden = true
            self.viewPhaseAndPrivate.isHidden = false
            self.descPhase.text = "12 слов"
           // self.labelPhase.text = RWWallet.getWallet().
            self.labelPhase.text = "123"
            break
        case .Keystore:
            self.viewKeystone.isHidden = false
            self.viewQR.isHidden = true
            self.viewPhaseAndPrivate.isHidden = true
            break
        case .Private:
            self.viewKeystone.isHidden = true
            self.viewQR.isHidden = true
            self.viewPhaseAndPrivate.isHidden = false
            self.descPhase.text = "privateKey"
            self.labelPhase.text = RWWallet.getWallet().getPrivateKey()
            break
        case .QR:
            self.viewKeystone.isHidden = true
            self.viewQR.isHidden = false
            self.viewPhaseAndPrivate.isHidden = true
            break
        }
    }
    
    
     @objc func exportWallet()->Void{
       
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
