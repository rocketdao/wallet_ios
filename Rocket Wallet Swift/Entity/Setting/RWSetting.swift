//
//  RWSetting.swift
//  Rocket Wallet Swift
//
//  Created by Иван Петрашко on 07.09.2018.
//  Copyright © 2018 Иван§ Петрашко. All rights reserved.
//

import UIKit
import RealmSwift
import KeychainSwift

class RWSetting: Object {
    @objc dynamic var passcode:Bool = false
    @objc dynamic var touchOrFace:Bool = false
    @objc dynamic  var mix = "asdasdsd"
    
    func getPasscodeString()->String{
        let keychain = KeychainSwift()
        return keychain.get("passcode")!
    }
    
    func setPasscodeString(str:String){
        let keychain = KeychainSwift()
        keychain.set(str, forKey: "passcode")
        
    }
    
    func getPasscodeEnabled()->Bool{
        if self.passcode && !self.touchOrFace {
            return true
        } else {
            return false
        }
    }
    
}
