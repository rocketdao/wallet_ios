//
//  RWSettingSequrityVC.swift
//  Rocket Wallet Swift
//
//  Created by Иван Петрашко on 05.09.2018.
//  Copyright © 2018 Иван§ Петрашко. All rights reserved.
//

import UIKit
import RealmSwift
import SmileLock
import LocalAuthentication

class RWSettingSequrityVC: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var table: UITableView!
    

    override func viewDidLoad() {
        super.viewDidLoad()

        title = "Setting"
        navigationController?.navigationBar.navigationStyle()
        self.table.delegate = self
        self.table.dataSource = self
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 2 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
            cell.textLabel?.text = "Change passcode"
            cell.accessoryType = .disclosureIndicator
            cell.textLabel?.font = UIFont.setFontStyle(style: .SemiLight, size: 17)
            cell.textLabel?.textColor = UIColor.darkGray
            
            if RWWallet.getWallet().setting.passcode {
                cell.textLabel?.alpha = 1
                  cell.isUserInteractionEnabled = true
            } else {
                cell.textLabel?.alpha = 0.5
                cell.isUserInteractionEnabled = false
            }
            
            return cell
        } else if indexPath.row == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "RWCellSettingSwitch", for: indexPath) as! RWCellSettingSwitch
            cell.title.text = "Touch ID"
            cell.switchbutton.setOn(RWWallet.getWallet().setting.touchOrFace, animated: false)
            cell.switchbutton.addTarget(self, action: #selector(self.tochfaceSwitch(switchOn:)), for: .valueChanged)
            
            if RWWallet.getWallet().setting.passcode {
                cell.title.alpha = 1
                cell.switchbutton.isEnabled = true
                 cell.isUserInteractionEnabled = true
            } else {
                cell.title.alpha = 0.5
                cell.switchbutton.isEnabled = false
                cell.isUserInteractionEnabled = false
            }
            
            return cell
        } else if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "RWCellSettingSwitch", for: indexPath) as! RWCellSettingSwitch
            cell.title.text = "Passcode"
            cell.switchbutton.setOn(RWWallet.getWallet().setting.passcode, animated: false)
            cell.switchbutton.addTarget(self, action: #selector(self.passcodeSwitch(switchOn:)), for: .valueChanged)
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
            return cell
        }
    }
    
    
    @objc func passcodeSwitch(switchOn:UISwitch){
        if !switchOn.isOn {
          let realm = try! Realm()
           try! realm.write {
                RWWallet.getWallet().setting.passcode = false
                RWWallet.getWallet().setting.touchOrFace = false
            table.beginUpdates()
            table.reloadRows(at: [IndexPath(row: 1, section: 0),IndexPath(row: 2, section: 0)], with: .automatic)
            table.endUpdates()
            }
        } else {
            let portfolio = UIStoryboard(name: "Sequrity", bundle: nil).instantiateViewController(withIdentifier: "RWSequrityKeyboardVC") as! RWSequrityKeyboardVC
            portfolio.textFirst = "Введите код пароль"
            portfolio.textTwo = "Повторите код пароль"
            var navigationFirst: UINavigationController! = nil
            navigationFirst = UINavigationController(rootViewController: portfolio)
            present(navigationFirst, animated: true)
        }
    }
    
    @objc func tochfaceSwitch(switchOn:UISwitch){
        if RWWallet.getWallet().setting.touchOrFace {
            let realm = try! Realm()
            try! realm.write {
                RWWallet.getWallet().setting.touchOrFace = false
            }
        } else {
            self.touchIdAction(switchOn: switchOn)
        }
    }
    
    func touchIdAction(switchOn:UISwitch) {
        
        print("hello there!.. You have clicked the touch ID")
        
        let myContext = LAContext()
        let myLocalizedReasonString = "Подтвердите действие"
        
        var authError: NSError?
        if #available(iOS 8.0, macOS 10.12.1, *) {
            if myContext.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &authError) {
                myContext.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: myLocalizedReasonString) { success, evaluateError in
                    
                    DispatchQueue.main.async {
                        if success {
                            let realm = try! Realm()
                            try! realm.write {
                                RWWallet.getWallet().setting.touchOrFace = true
                            }
                        } else {
                            switchOn.setOn(false, animated: true)
                            self.alert(title: "Error", message: "User did not authenticate successfully")
                            // User did not authenticate successfully, look at error and take appropriate action
                          //  self.successLabel.text = "Sorry!!... User did not authenticate successfully"
                        }
                    }
                }
            } else {
                switchOn.setOn(false, animated: true)
                self.alert(title: "Error", message: "Could not evaluate policy")
                // Could not evaluate policy; look at authError and present an appropriate message to user
               // successLabel.text = "Sorry!!.. Could not evaluate policy."
            }
        } else {
            // Fallback on earlier versions
            switchOn.setOn(false, animated: true)
            self.alert(title: "Error", message: "This feature is not supported.")
         //   successLabel.text = "Ooops!!.. This feature is not supported."
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 2 {
            let portfolio = UIStoryboard(name: "Sequrity", bundle: nil).instantiateViewController(withIdentifier: "RWSequrityKeyboardVC") as! RWSequrityKeyboardVC
            portfolio.textFirst = "Введите старый код пароль"
            portfolio.textTwo = "Введите новый код пароль"
            portfolio.change = true
            var navigationFirst: UINavigationController! = nil
            navigationFirst = UINavigationController(rootViewController: portfolio)
            present(navigationFirst, animated: true)
        }
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.table.reloadData()
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

