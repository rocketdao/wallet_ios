//
//  RWBill.swift1Rocket Wallet Swift
//
//  Created by Иван Петрашко on 14.08.2018.
//  Copyright © 2018 Иван§ Петрашко. All rights reserved.
//

import UIKit
import Web3
import RealmSwift

enum RWTaxFeeSpeed {
    case Slow
    case Normal
    case Fast
}

class RWBill: NSObject {
    @objc dynamic var token: RWToken! = nil
    @objc dynamic var sum = ""
    
    @objc dynamic var address = ""
    @objc dynamic var revers = false
    var gasPrice:BigUInt = 0
    var gasPriceOnline:BigUInt = 0
    var gasLimit:BigUInt = 0

    func getTxFeeETH()->Double{
        return (Double(self.gasPriceOnline) * Double(self.gasLimit)) / pow(10,Double(18))
    }
    
    func getTxFeeETHString()->String{
        return String(format: "ETH %f", getTxFeeETH())
    }
    
    func getTxFeeCurrency()->String{
        let realm = try! Realm()
        let rateOnline = realm.objects(RWRate.self).filter(NSPredicate(format: "tokenSymbol == %@ && online == %@", "ETH", NSNumber(value: true))).first
        if rateOnline != nil {
            return String(format: "$%.2f", getTxFeeETH() * rateOnline!.rate.doubleValue)
        } else {
            return "0"
        }
    }
    
    func getTaxFeeSpeedLabel() -> String {
        switch getTaxFeeSpeedState() {
        case .Slow:
            return "Slow"
        case .Fast:
            return "Fast"
        case .Normal:
            return "Normal"
        default:
            return "Normal"
        }
    }
    
    func getTaxFeeSpeedColor() -> UIColor {
        switch getTaxFeeSpeedState() {
        case .Slow:
            return UIColor(red:0.98, green:0.27, blue:0, alpha:1)
        case .Fast:
            return UIColor(red:0, green:0.94, blue:0.51, alpha:1)
        case .Normal:
            return UIColor(red:0.56, green:0.56, blue:0.58, alpha:1)
        default:
            return UIColor.black
        }
    }
    
    func getTaxFeeSpeedState()->RWTaxFeeSpeed{
        if gasPriceOnline<gasPrice {
            return .Slow
        } else if gasPriceOnline > gasPrice*2{
            return .Fast
        } else if gasPriceOnline >= gasPrice {
            return .Normal
        } else {
            return .Normal
        }
    }
    
    func getAllSumDouble()->Double{
        if revers
        {
            return getInCurrency()
        } else {
            return getSumDouble()
        }
    }
    
    func getSumDouble()->Double{
        var sum2 = sum
        if sum2 == "" {
            sum2 = "0"
        }
        return Double(sum2)!
    }
    
    func getAllSumAndTax()->String{
        if token.symbol == "ETH" {
            return String(format: "%@ %f",token.symbol, getAllSumDouble() + getTxFeeETH())
        } else {
            return String(format: "%@ %f",token.symbol, getAllSumDouble())
        }
    }
    
    func getAllSumAndTaxForBill()->String{
        if token.symbol == "ETH" {
            return String(format: "%@ %f",token.symbol, getAllSumDouble() + getTxFeeETH())
        } else {
            return String(format: "%@ %f + %@",token.symbol, getAllSumDouble(),getTxFeeETHString())
        }
    }
    
    func getAllSum()->String{
        if token.symbol == "ETH" {
            return String(format: "%@ %f",token.symbol, getAllSumDouble())
        } else {
            return String(format: "%@ %f",token.symbol, getAllSumDouble())
        }
    }
    
    func getAllSumInCurrency()->String{
        let realm = try! Realm()
        let rateOnline = realm.objects(RWRate.self).filter(NSPredicate(format: "tokenSymbol == %@ && online == %@", self.token.symbol, NSNumber(value: true))).first
        if rateOnline != nil {
            return String(format: "USD %.2f", getAllSumDouble() * rateOnline!.rate.doubleValue + getTxFeeETH() * RWToken.getEthRate())
        } else {
            return "0"
        }
    }
    
    func getInCurrency()->Double{
        return getSumDouble() / token.getRate()!
    }
    
    func getInCurrency()->String{
        if !revers {
            return String(format: "%.2f", token.getRate()! * getSumDouble())
        } else {
            return String(format: "%f", getSumDouble() / token.getRate()!)
        }
    }
    
    func getAmountInCurrenctBill()->String{
        return String(format: "%.2f", token.getRate()! * getAllSumDouble())
    }
    
    func reversChange(){
        sum = getInCurrency()
        revers = !revers
    }
    
    func useAll(){
        if !revers {
            sum = token.getBalanceString()!
        } else {
            sum = String(format: "%f",token.getInCurrencyDouble(true))
        }
    }
    
    func validation(block: @escaping () -> Void, blockError: @escaping (_ string: String?) -> Void){
        if token.symbol == "ETH" {
            if token.balance.doubleValue >= getAllSumDouble() + getTxFeeETH() {
                block()
            } else {
                blockError("Сумма привышает ваш баланс")
            }
        } else {
            print(String(format: "%f",self.token.balance.doubleValue))
            print(String(format: "%f",self.getAllSumDouble()))
            if self.token.balance.doubleValue.isLess(than:self.getAllSumDouble())  {
                print("капуста 1")
            } else if self.token.balance.doubleValue > self.getAllSumDouble(){
                 print("капуста 2")
            } else  if self.token.balance.doubleValue == self.getAllSumDouble(){
                 print("капуста 3")
            } else {
                print("капуста 4")
            }
                if self.token.balance.doubleValue < self.getAllSumDouble(){
                blockError("У вас токенов меньше")
                } else if RWWallet.getWallet().getETHToken().balance.doubleValue < self.getTxFeeETH(){
                blockError("не хватает на газ")
            } else {
                block()
            }
        }
    }
    

    
    func getGasPrice(){
        let web3 = Web3(rpcURL: Constants.URL_NODA)
        web3.eth.gasPrice { (response) in
            if response.status.isSuccess {
                print(response.result)
                self.gasPrice = (response.result?.quantity)!
                self.gasPriceOnline = self.gasPrice
                print("OK")
                DispatchQueue.main.async {
                    self.getGasLimit()
                }
            }
        }
    }
    
    func getSumTransactionSendEther()->BigUInt{
        return BigUInt(getAllSumDouble() * pow(10,Double(18)))
    }
    
    
    
    func sendEth(block: @escaping (_ sendTrans:Bool) -> Void, andError blockError: @escaping (_ string: String?) -> Void){
        let web3 = Web3(rpcURL: Constants.URL_NODA)
        
        let p = try? EthereumPrivateKey(
            hexPrivateKey: RWWallet.getWallet().getPrivateKey()
        )

        guard let privateKey = p else {
            return
        }
          print(self.address)
        print(privateKey.address.hex(eip55: false))
        print(privateKey.hex())
       // let privateKey = try! EthereumPrivateKey(hexPrivateKey: "0xa26da69ed1df3ba4bb2a231d506b711eace012f1bd2571dfbfff9650b03375af")
        firstly {
            web3.eth.getTransactionCount(address: privateKey.address, block: .latest)
            }.then { nonce in
               // print(nonce)
                try EthereumTransaction(
                    nonce: nonce,
                    gasPrice: EthereumQuantity(quantity: self.gasPrice),
                    gas: EthereumQuantity(quantity: self.gasLimit),
                    from:privateKey.address,
                    to: EthereumAddress(hex: self.address, eip55: false),
                    value: EthereumQuantity(quantity: self.getSumTransactionSendEther())
                ).sign(with: privateKey, chainId: 42).promise
            }.then { tx in
                web3.eth.sendRawTransaction(transaction: tx)
            }.done { hash in
                 block(true)
            }.catch { error in
                 blockError(error.localizedDescription)
            }
    }
    
    func errrorSend(str:String){
        
    }
    
    func sendToken(block: @escaping (_ sendTrans:Bool) -> Void, andError blockError: @escaping (_ string: String?) -> Void){
        let web3 = Web3(rpcURL: Constants.URL_NODA)
        
        let p = try? EthereumPrivateKey(
            hexPrivateKey: RWWallet.getWallet().getPrivateKey()
        )
        guard let privateKey = p else {
            return
        }
        print(privateKey.address.hex(eip55: false))
        do{
            
            let contractAddress = try EthereumAddress(hex: token.address, eip55: false)
            let contract = web3.eth.Contract(type: GenericERC20Contract.self, address: contractAddress)
            
            firstly {
                web3.eth.getTransactionCount(address: privateKey.address, block: .latest)
                }.then { nonce in
                    try contract.transfer(to: EthereumAddress(hex: self.address, eip55: true), value: self.getSumTransactionSendEther()).createTransaction(
                        nonce: nonce,
                        from: privateKey.address,
                        value: 0,
                        gas: EthereumQuantity(quantity:self.gasLimit),
                        gasPrice: EthereumQuantity(quantity: self.gasPrice)
                        )!.sign(with: privateKey, chainId: 42).promise
                }.then { tx in
                    web3.eth.sendRawTransaction(transaction: tx)
                }.done { txHash in
                    print(txHash)
                  //  self.sendComplete()
                    block(true)
                }.catch { error in
                   // self.errrorSend(str: error.localizedDescription)
                     blockError(error.localizedDescription)
            }
        } catch {
            self.errrorSend(str: "error")
        }
    }
    
    func sendComplete(){
        
    }
    
    func sendBill(block: @escaping (_ sendTrans:Bool) -> Void, andError blockError: @escaping (_ string: String?) -> Void)->Void{
        if token.symbol == "ETH" {
           sendEth(block: block, andError: blockError)
        } else {
           sendToken(block: block, andError: blockError)
        }
    }
    func updateGasLimit(gas:BigUInt){
        do{
            let realm = try! Realm()
            // realm.beginWrite()
            try realm.write {
                self.gasLimit = gas
                // realm.commitWrite()
            }
        } catch {
            
        }
        
    }
    
    func getGasLimit(){
         let web3 = Web3(rpcURL: Constants.URL_NODA)
    // do {
        if self.token.symbol == "ETH" {
        firstly {
            try EthereumCall(
                from: EthereumAddress(hex: "0x9f00678429cb91bc264462c36c23e2031176f645", eip55: false),
                to: EthereumAddress(hex: "0x2e704bf506b96adac7ad1df0db461344146a4657", eip55: false),
                gas: nil,
                gasPrice: nil,
                value: 10,
                data: EthereumData(
                    ethereumValue: "0xaa1e84de000000000000000000000000000000000000000000000000000000000000002000000000000000000000000000000000000000000000000000000000000000046461766500000000000000000000000000000000000000000000000000000000"
                )
                ).promise

            }.then { call in
                web3.eth.estimateGas(call: call)
            }.done { quantity in
                let expectedQuantity: EthereumQuantity = try .string("0x58dc")
                print(expectedQuantity.quantity)
                self.gasLimit = expectedQuantity.quantity
                NotificationCenter.default.post(name: NSNotification.Name("sliderUpdate"), object: nil)
            }.catch { error in
                print(error)
            }
        } else {
            do{
                let contractAddress = try EthereumAddress(hex:  self.token.address, eip55: false)
                let contract = web3.eth.Contract(type: GenericERC20Contract.self, address: contractAddress)
                let invocation = contract.transfer(to: try EthereumAddress(hex: "0x2e704bf506b96adac7ad1df0db461344146a4657", eip55: false), value: self.getSumTransactionSendEther())
                    firstly {
                        invocation.estimateGas(from: try EthereumAddress(hex: RWWallet.getWallet().publicKey, eip55: false), value: 0)
                        }.done { gas in
                            print(gas)
                            print("gas")
                            DispatchQueue.main.async {
                                self.updateGasLimit(gas: gas.quantity)
                                NotificationCenter.default.post(name: NSNotification.Name("sliderUpdate"), object: nil)
                            }
                            
                         //   done()
                        }.catch { error in
                             print("errror")
                            print(error)
                            DispatchQueue.main.async {
                                self.updateGasLimit(gas: 100000)
                            }
                           // fail(error.localizedDescription)
                    }
            //try EthereumCall(
         //   web3.eth.estimate
            } catch {
                print("sipez")
            }
        }
    }
}
