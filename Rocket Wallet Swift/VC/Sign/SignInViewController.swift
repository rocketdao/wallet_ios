//
//  SignInViewController.swift
//  Rocket Wallet Swift
//
//  Created by Иван Петрашко on 20.07.2018.
//  Copyright © 2018 Иван§ Петрашко. All rights reserved.
//

import UIKit
import Web3

class SignInViewController: UIViewController {

    @IBOutlet weak var buttonCreate: UIButton!
    @IBOutlet weak var buttonImport: UIButton!
    @IBOutlet weak var backGradient: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
    
        buttonCreate.setButtonStyle(style: .Sign)
        buttonCreate.setTitle("Create new wallet", for: .normal)
        
        
        buttonImport.setButtonStyle(style: .Sign)
        buttonImport.setTitle("Import wallet", for: .normal)
        
        buttonCreate.addTarget(self, action: #selector(self.createWallet), for: .touchUpInside)
        buttonImport.addTarget(self, action: #selector(self.importWallet), for: .touchUpInside)
        view.layoutIfNeeded()
        let gradient = CAGradientLayer()
        gradient.frame = backGradient.bounds
        gradient.colors = [(UIColor.rwColorBlueButton().cgColor), (UIColor.white.cgColor)]
        backGradient.layer.insertSublayer(gradient, at: 0)


        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func createWallet() {
        openTabView()
    }
    
    @objc func importWallet(){
        let portfolio = UIStoryboard(name: "ImportWallet", bundle: nil).instantiateViewController(withIdentifier: "RWImportWalletVC") as! RWImportWalletVC
        var navigationFirst: UINavigationController! = nil
        navigationFirst = UINavigationController(rootViewController: portfolio)
        self.present(navigationFirst, animated: true, completion: nil)
    }
    
    func openTabView() {
        let tabBar = RWTabBar()
        //or the homeController
        do {
        //tabBar.privatekey = try EthereumPrivateKey().hex()
        tabBar.privatekey = ""
        tabBar.setVC()
        present(tabBar, animated: false)
        } catch {
            
        }
    }

    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
