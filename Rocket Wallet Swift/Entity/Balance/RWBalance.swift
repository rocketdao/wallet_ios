//
//  RWBalance.swift
//  Rocket Wallet Swift
//
//  Created by Иван Петрашко on 04.09.2018.
//  Copyright © 2018 Иван§ Петрашко. All rights reserved.
//

import UIKit
import Realm
import RealmSwift

class RWBalanceToken:Object {
    @objc dynamic var token: String = ""
    @objc dynamic var balance:Double = 0
    @objc dynamic var rate:Double = 0
    
    class func getBalanceTokenFromJson(json:Dictionary<String, Any>)->RWBalanceToken{
        let balance = RWBalanceToken()
        balance.token = json["token"] as! String
        balance.rate = json["rate"] as! Double
        balance.balance = json["balance"] as! Double
        return balance
    }
}

class RWBalance: Object {
    @objc dynamic var date: Date = Date()
    @objc dynamic var totalUSD:Double = 0
    var tokenTypes:List<RWBalanceToken> = List.init()
  /*
    func getChange24hForToken(token:String)->Int{
        
    }
*/
    class func getBalanceList(tokens:List<RWToken>, dateFrom:Date,dateTo:Date,points:NSNumber ,block: @escaping (_ balance: List<RWBalance>) -> Void, andError blockError: @escaping (_ string: String?) -> Void){
        var tokenString = ""
        for token: RWToken in tokens {
            tokenString = "\(tokenString)&tokenTypeList=\(token.symbol)"
        }
        API.sharedInstance.request(url: String(format: "tokens/range-balance?address=%@&fromDate=%@&toDate=%@%@&points=%@",RWWallet.getWallet().publicKey,dateFrom.getDateString(byFormat: Constants.DATETIME_SERVER_FORMAT)!,dateTo.getDateString(byFormat: Constants.DATETIME_SERVER_FORMAT)!,tokenString,points), completion: { (dict:Any) in
            print(dict)
            let arr:List<RWBalance> = List.init()
            let arrDict = dict as? [NSDictionary]
            //if
            if arrDict != nil {
            for dict in arrDict!{
                arr.append(RWBalance.getBalanceFromJson(json: dict as! Dictionary<String, Any>))
            }
                   block(arr)
            } else {
                 blockError("Error");
            }
            
            if Constants.KOVAN {
                block(List.init())
            }
         
        }) { (string) in
            blockError(string);
        }
    }
    
    class func getBalanceFromJson(json:Dictionary<String, Any>)->RWBalance{
        let balance = RWBalance()
        let unixTimestamp = json["date"] as! Int
        let date = Date(timeIntervalSince1970: TimeInterval(unixTimestamp/1000))
        balance.date = date
        balance.totalUSD = json["totalUsd"] as! Double
        
        
        let results = json["balance"] as! [[String:Any]]
        
        for balanceToken in results {
             balance.tokenTypes.append(RWBalanceToken.getBalanceTokenFromJson(json: balanceToken))
        }
       
        return balance
    }
    

class func getBalance(tokens:List<RWToken>, date:Date,block: @escaping (_ balance: RWBalance) -> Void, andError blockError: @escaping (_ string: String?) -> Void){
    var tokenString = ""
    for token: RWToken in tokens {
        tokenString = "\(tokenString)&tokenTypeList=\(token.symbol)"
    }

    API.sharedInstance.request(url: String(format: "tokens/balance?address=%@&date=%@%@",RWWallet.getWallet().publicKey,date.getDateString(byFormat: Constants.DATETIME_SERVER_FORMAT)!,tokenString), completion: { (dict:Any) in
        let dict2 = dict as? Dictionary<String, Any>
        block(RWBalance.getBalanceFromJson(json: dict2!))
    }) { (string) in
         blockError(string);
    }
}
}
