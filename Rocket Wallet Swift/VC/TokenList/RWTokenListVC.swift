
//
//  RWTokenListVC.swift
//  Rocket Wallet Swift
//
//  Created by Иван Петрашко on 24.07.2018.
//  Copyright © 2018 Иван§ Петрашко. All rights reserved.
//

import UIKit
import RealmSwift

class RWTokenListVC: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {

    @IBOutlet weak var table: UITableView!
    var tokens: List<RWToken> = List.init()
    @IBOutlet weak var search: UISearchBar!
    var wallet: RWWallet!
    @IBOutlet weak var bottheight: NSLayoutConstraint!
    var arrSelected:Array<RWToken>!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.navigationStyle()
        var cancel = UIBarButtonItem(title: "Cancel", style: .done, target: self, action: #selector(self.cancelFunc))
        navigationItem.leftBarButtonItem = cancel
        var done = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(self.doneFunc))
        navigationItem.rightBarButtonItem = done
        
      //  self.wallet = 
        
        title = "Add Token"
        table.delegate = self
        table.dataSource = self
       // tokens = wallet.getTokenAllSearch("")
        tokens = wallet.getTokenAllSearch("")
        //self.tokensSearch = self.tokens;
        table.reloadData()
        table.setEditing(true, animated: false)
        arrSelected = []

        for token: RWToken in RWWallet.getWallet().tokens {
                arrSelected.append(token)
        }
        
        selectedRowBYArray()
       
        search.showsCancelButton = false
        search.delegate = self
        
        search.backgroundColor = UIColor.white
        search.tintColor = UIColor.black
        search.barTintColor = UIColor.white
        //NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow(_:)), name: .UIKeyboardWillShow, object: nil)
        //NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillBeHidden(_:)), name: .UIKeyboardWillHide, object: nil)
        //NotificationCenter.default.addObserver(self, selector: #selector(self.reloadTableImage), name: NSNotification.Name("ReloadImageTokenList"), object: nil)
        UIBarButtonItem.appearance(whenContainedInInstancesOf: [UISearchBar.self]).tintColor = UIColor.gray
        search.placeholder = "Search"
        // Do any additional setup after loading the view.
    }
    
    func selectedRowBYArray() -> Void {
        for i in 0..<tokens.count {
            let arrtok = arrSelected.filter({$0.symbol == tokens[i].symbol})
            if arrtok.count > 0 {
                let indexPath = IndexPath(row: i, section: 0)
                table.selectRow(at: indexPath, animated: true, scrollPosition: .none)
            }
        }
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        arrSelected.append(tokens[indexPath.row])
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let news = arrSelected.filter({$0.symbol == tokens[indexPath.row].symbol})
        if news.count>0{
            if let anObject = news.first {
                while let elementIndex = arrSelected.index(of: anObject) { arrSelected.remove(at: elementIndex) }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
        return UITableViewCellEditingStyle(rawValue: 3)!
    }
    
  @objc func doneFunc() {
    for token: RWToken in tokens {
            let news = arrSelected.filter({$0.symbol == token.symbol})
            if news.count>0{
                let realm = try! Realm()
                if realm.objects(RWWallet.self).first?.tokens.filter(NSPredicate(format: "symbol == %@", (news.first?.symbol)!)).first == nil {
                    try! realm.write {
                        token.sortNumber = RWWallet.getWallet().tokens.count
                        RWWallet.getWallet().tokens.append(token)
                    }
                }
            } else {
                let realm = try! Realm()
                if  realm.objects(RWWallet.self).first?.tokens.filter(NSPredicate(format: "symbol == %@", (token.symbol))).count == 1 {
                    try! realm.write {
                        RWWallet.getWallet().tokens.remove(at: RWWallet.getWallet().tokens.index(matching: NSPredicate(format: "symbol == %@", (token.symbol)))!)
                    }
                }
            }
        }
        RWWallet.getWallet().sortNumberReset()
        navigationController?.dismiss(animated: true)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! RWCellTokenList
        cell.name.text = tokens[indexPath.row].name
        cell.tintColor = UIColor.rwColorBlueButton()
        cell.logo.image = tokens[indexPath.row].logo?.getImage(nil, andNotification: "ReloadImageTokenList")
        //if let aCell = cell {
        return cell
       // }
        //return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tokens.count
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        tokens = wallet.getTokenAllSearch(searchText)
        table.reloadData()
        selectedRowBYArray()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        search.resignFirstResponder()
        print("log3")
        search.showsCancelButton = false
        search.text = ""
        // [self.search becomeFirstResponder];
        tokens = wallet.getTokenAllSearch("")
        table.reloadData()
        selectedRowBYArray()
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        print("log1")
        search.showsCancelButton = true
    }
    
    @objc func cancelFunc() {
        navigationController?.dismiss(animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
