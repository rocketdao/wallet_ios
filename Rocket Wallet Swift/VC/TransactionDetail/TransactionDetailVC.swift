//
//  TransactionDetailVC.swift
//  Rocket Wallet Swift
//
//  Created by Иван Петрашко on 22.08.2018.
//  Copyright © 2018 Иван§ Петрашко. All rights reserved.
//

import UIKit

class TransactionDetailVC: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "RWCellTransactionDate", for: indexPath) as! RWCellTransactionDate
            cell.title1.text = "Date"
            cell.title2.text = self.transaction.date.description
            return cell;
        } else if indexPath.row == 1{
            let cell = tableView.dequeueReusableCell(withIdentifier: "RWCellTransactionDate", for: indexPath) as! RWCellTransactionDate
            cell.title1.text = "Status"
            cell.title2.text = String(format: "%d confirmations", self.transaction.confirmations)
            return cell;
        } else if indexPath.row == 2{
            let cell = tableView.dequeueReusableCell(withIdentifier: "RWCellTransactionSum", for: indexPath) as! RWCellTransactionSum
            
            if self.transaction.received {
                cell.title1.textColor = UIColor(red:0, green:0.94, blue:0.51, alpha:1)
                cell.title1.text = "Received"
            } else {
                cell.title1.textColor = UIColor(red:0.98, green:0.27, blue:0, alpha:1)
                cell.title1.text = "Send"
            }
            cell.title3.text = String(format: "%@ %@",self.transaction.tokenType, self.transaction.value!)
            cell.title2.text = String(format: "USD %.2f",(self.transaction.usdval?.doubleValue)!)

            return cell;
        } else if indexPath.row == 3{
            let cell = tableView.dequeueReusableCell(withIdentifier: "RWCellTransactionSum", for: indexPath) as! RWCellTransactionSum
            cell.title1.textColor = UIColor(red:0.98, green:0.27, blue:0, alpha:1)
            cell.title1.text = "Transaction fee"
            
            cell.title2.text = String(format: "USD %.2f", (self.transaction.fee?.doubleValue)! * RWToken.getEthRate())
            cell.title3.text = String(format: "ETH %.4f",(self.transaction.fee?.doubleValue)!)
            return cell;
        } else if indexPath.row == 4{
            let cell = tableView.dequeueReusableCell(withIdentifier: "RWCellTransactionDate", for: indexPath) as! RWCellTransactionDate
            cell.title1.text = "Transaction id"
            cell.title2.text = self.transaction.hashString
            return cell;
        }  else {
            return tableView.dequeueReusableCell(withIdentifier: "RWCellTransactionDate", for: indexPath) as! RWCellTransactionDate
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
       return 55
    }
    
    
    
    @IBOutlet weak var table: UITableView!
    var transaction: RWTransaction!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Transaction Details"
        self.table.delegate = self
        self.table.dataSource = self
        
        self.table.separatorStyle = .none
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
