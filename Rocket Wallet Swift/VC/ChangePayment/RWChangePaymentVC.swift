//
//  RWChangePaymentVC.swift
//  Rocket Wallet Swift
//
//  Created by Иван Петрашко on 14.08.2018.
//  Copyright © 2018 Иван§ Петрашко. All rights reserved.
//

import UIKit

class RWChangePaymentVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var bill:RWBill!
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return RWWallet.getWallet().getSortTokens().count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellHeader", for: indexPath) as! RWCellTokenHeader
        cell.viewLabel.isHidden = true;
        cell.eth.text = String(format: "%@ %@",RWWallet.getWallet().getSortTokens()[indexPath.row].symbol,RWWallet.getWallet().getSortTokens()[indexPath.row].getBalanceString()!)
        cell.sum.text = RWWallet.getWallet().getSortTokens()[indexPath.row].getInCurrencyString()
        cell.imageProfile.image = RWWallet.getWallet().getSortTokens()[indexPath.row].logo?.getImage(tableView, andNotification: nil)
        cell.sumchange.isHidden = true
        cell.percentchange.isHidden = true
        cell.currency.isHidden = true
        cell.h24.isHidden = true
        return cell;
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 89
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        NotificationCenter.default.post(name: NSNotification.Name("setPaymentToken"), object: RWWallet.getWallet().getSortTokens()[indexPath.row])
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBOutlet weak var table: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Select Token"
        navigationController?.navigationBar.navigationStyle()
        view.backgroundColor = UIColor.rwColorGrayBackground()
        table.dataSource = self as UITableViewDataSource
        table.delegate = self as UITableViewDelegate
        
        table.register(UINib(nibName: "Cell", bundle: nil), forCellReuseIdentifier: "CellHeader")
        table.delegate = self
        table.dataSource = self
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
