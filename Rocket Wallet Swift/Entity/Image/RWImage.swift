//
//  RWImage.swift
//  Rocket Wallet Swift
//
//  Created by Иван Петрашко on 20.07.2018.
//  Copyright © 2018 Иван§ Петрашко. All rights reserved.
//

import UIKit
import Realm
import RealmSwift

class RWImage: Object {
    @objc dynamic var data: Data = Data()
    @objc dynamic var url = ""
    
    func getImage(_ table: UITableView?, andNotification notificationName: String?) -> UIImage? {
       // print(String(format: "image %@", self))
        // print(String(format: "image_DATA %@", String(decoding: data, as: UTF8.self)))
        if String(decoding: data, as: UTF8.self) != "Load" && data.bytes.count>0 {
            return UIImage(data: data)
        } else if self.url == "" {
        //   print("нет урл")
        } else {
          //    print("есть урл")
            var next = false
          //  if (data != nil) {
                if String(decoding: data, as: UTF8.self) != "Load" {
                    next = true
                }
        /*    } else {
                next = true
            }*/
            if next {
                
              /*  let realm = try! Realm()
                try! realm.write {
                    self.data = "Load".data(using: .utf8)!
                }*/
                
                getImageFromUrl({ image in
                    let realm = try! Realm()
                    try! realm.write {
                        self.data = image!
                    }
                    if (table != nil) {
                        table?.reloadData()
                    } else if (notificationName != nil) {
                        NotificationCenter.default.post(name: NSNotification.Name("notificationName"), object: nil)
                    }
                }, andError: { string in
                })
            }

        }
        return nil
    }

    
    func getImageFromUrl(_ block: @escaping (_ image: Data?) -> Void, andError blockError: @escaping (_ string: String?) -> Void) {
     //   RLMRealm.default().beginWriteTransaction()
         let realm = try! Realm()
        try! realm.write {
             data = NSKeyedArchiver.archivedData(withRootObject: "Load")
        }
       
    //    RLMRealm.default().commitWriteTransaction()
        let url = self.url.copy()
        
        DispatchQueue.global().async {
            let data = try? Data(contentsOf: URL(string:url as! String)!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
            DispatchQueue.main.async {
                if((data) != nil){
                  block(data!)
                }
              //  imageView.image = UIImage(data: data!)
            }
        }

        
        
        /*
        URLSession.shared.dataTask(with: url) { data, response, error in
            completion(data, response, error)
            }.resume()
        
        
        DispatchQueue.global(qos: .default).async(execute: {
            let imageURL = URL(string: url as! String)
            var imageData: Data? = nil
            if imageURL != nil {
                imageData = Data(contentsOf: imageURL!)
            }
            DispatchQueue.main.sync(execute: {
                block(imageData)
            })
        })*/
    }
    /*
    func downloadImage(url: URL) {
        print("Download Started")
        
        URLSession.shared.dataTask(with: url) { data, response, error in
            completion(data, response, error)
            }.resume()
        
        
        getDataFromUrl(url: url) { data, response, error in
            guard let data = data, error == nil else { return }
            print(response?.suggestedFilename ?? url.lastPathComponent)
            print("Download Finished")
            DispatchQueue.main.async() {
                self.imageView.image = UIImage(data: data)
            }
        }
    }*/

}
