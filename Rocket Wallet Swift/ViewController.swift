//
//  ViewController.swift
//  Rocket Wallet Swift
//
//  Created by Иван Петрашко on 19.07.2018.
//  Copyright © 2018 Иван§ Петрашко. All rights reserved.
//

import UIKit
import Web3

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        let web3 = Web3(rpcURL: Constants.URL_NODA)
        
        firstly {
            web3.clientVersion()
            }.done { version in
                print(version)
            }.catch { error in
                print("Error")
        }
       /* do {
            let contractAddress = try EthereumAddress(hex: "0x9f00678429cb91bc264462c36c23e2031176f645", eip55: true)
            contractAddress
        } catch {
              print(error)
        }*/
        do {
        let contractAddress = try EthereumAddress(hex: "0x9f00678429cb91bc264462c36c23e2031176f645", eip55: false)
        web3.eth.getBalance(address:contractAddress, block:  .latest) { response  in
            if response.status.isSuccess == true {
                print(response.result?.quantity)
            }
        }
        } catch {
            
        }
        
    
        
        do {
        let contractAddress = try EthereumAddress(hex: "0xab95e915c123fded5bdfb6325e35ef5515f1ea69", eip55: false)
        let contract = web3.eth.Contract(type: GenericERC20Contract.self, address: contractAddress)
            firstly {
                contract.name().call()
                }.done { outputs in
                    print(outputs["_name"])
                }.catch { error in
                    print(error)
            }
            //   print()
            firstly {
                try contract.balanceOf(address: EthereumAddress(hex: "0x9f00678429cb91bc264462c36c23e2031176f645", eip55: false)).call()
                }.done { outputs in
                    print(outputs["_balance"] as? BigUInt)
                }.catch { error in
                    print(error)
            }
        } catch {
            
        }
        /*
      //  let web3 = Web3(rpcURL: "https://mainnet.infura.io/<your_infura_id>")
        do {
             let contractAddress = try EthereumAddress(hex: "0xab95e915c123fded5bdfb6325e35ef5515f1ea69", eip55: false)
            
            //
             let contract = web3.eth.Contract(type: GenericERC20Contract.self, address: contractAddress)
            //all fine with jsonData here
            firstly {
                try contract.balanceOf(address: EthereumAddress(hex: "0x9f00678429cb91bc264462c36c23e2031176f645", eip55: true)).call()
                }.done { outputs in
                    print(outputs["_balance"] as? BigUInt)
                }.catch { error in
                    print(error)
            }
            
        } catch {
            //handle error
            print(error)
        }*/
       
        
       
        
       
        
        // Get balance of some address
       
        /*
        let contractAddress = try? EthereumAddress(hex: "0x86fa049857e0209aa7d9e616f7eb3b3b78ecfdb0", eip55: true)
        let contract = web3.eth.Contract(type: GenericERC20Contract.self, address: contractAddress)
        
        // Get balance of some address
        firstly {
            try contract.balanceOf(address: EthereumAddress(hex: "0x3edB3b95DDe29580FFC04b46A68a31dD46106a4a", eip55: true)).call()
            }.done { outputs in
                print(outputs["_balance"] as? BigUInt)
            }.catch { error in
                print(error)
        }
        */
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

