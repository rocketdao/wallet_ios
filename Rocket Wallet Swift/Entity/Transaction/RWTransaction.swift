//
//  RWTransaction.swift
//  Rocket Wallet Swift
//
//  Created by Иван Петрашко on 21.08.2018.
//  Copyright © 2018 Иван§ Петрашко. All rights reserved.
//

import UIKit
import Realm
import RealmSwift

class RWTransaction: Object {
    
    var tokenType = ""
    var timestamp: Int = 0
    var addressTo = ""
    var addressFrom = ""
    var hashString = ""
    var value: NSNumber?
    var usdval: NSNumber?
    var received = false
    var confirmations: Int = 0
    var date: Date = Date()
    var fee: NSNumber?
    
    class func getTransaction(tokens:List<RWToken>, fromDate:Date, todate:Date,block: @escaping (_ wallet: List<RWTransaction>) -> Void, andError blockError: @escaping (_ string: String?) -> Void){
        var tokenString = ""
        for token: RWToken in tokens {
            tokenString = "\(tokenString)&tokenTypeList=\(token.symbol)"
        }
        
        //RWWallet.getWallet().publicKey
        
        API.sharedInstance.request(url: String(format: "transactions?address=%@&fromDate=%@&toDate=%@&%@",RWWallet.getWallet().publicKey,fromDate.getDateString(byFormat: Constants.DATETIME_SERVER_FORMAT)!,todate.getDateString(byFormat: Constants.DATETIME_SERVER_FORMAT)!,tokenString), completion: { (dict:Any) in
            let arr:List<RWTransaction> = List.init()
            
            var arrDict = dict as? [NSDictionary]
            arrDict?.reverse()
            if (arrDict != nil) {
            for dict in arrDict!{
                print(dict)
                let transaction = RWTransaction()
                transaction.tokenType = dict["tokenType"] as! String
                transaction.timestamp = dict["timestamp"] as! Int
                transaction.addressTo = dict["addressTo"] as! String
                transaction.addressFrom = dict["addressFrom"] as! String
                transaction.value = dict["value"] as! NSNumber
                transaction.hashString = dict["hash"] as! String
                transaction.received = dict["received"] as! Bool
                transaction.confirmations = dict["confirmations"] as! Int
                transaction.fee = dict["fee"] as! NSNumber
                transaction.date = Date.getFromServer(str: dict["date"] as! String)
                transaction.usdval = dict["usdVal"] as! NSNumber
                arr.append(transaction)
            }
            }
            block(arr)
        }) { (str:String) in
            blockError(str);
        }
    }

    
  //  transactions?address=0x9f00678429cb91bc264462c36c23e2031176f645&fromDate=2016-06-19T09:00:00.000Z&toDate=2018-06-19T09:00:00.000Z&tokenTypeList=eth

}
