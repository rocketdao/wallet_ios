//
//  RWTabBar.swift
//  Rocket Wallet Swift
//
//  Created by Иван Петрашко on 20.07.2018.
//  Copyright © 2018 Иван§ Петрашко. All rights reserved.
//

import UIKit

class RWTabBar: UITabBarController {
    
    var tabBarItemArray:NSMutableArray = []
    var privatekey =  ""
    func setVC() {
        tabBarItemArray = []
        let portfolio = UIStoryboard(name: "Portfolio", bundle: nil).instantiateViewController(withIdentifier: "RWPortfolioVC") as! RWPortfolioVC
        portfolio.privateKey = privatekey
        var navigationFirst: UINavigationController! = nil
        navigationFirst = UINavigationController(rootViewController: portfolio)
        navigationFirst!.tabBarItem = UITabBarItem(title: "Portfolio", image: UIImage(named: "profile"), tag: 0)
        navigationFirst!.tabBarItem.selectedImage = UIImage(named: "profile")
        tabBarItemArray.add(navigationFirst)
        
        
       
        let setting = UIStoryboard(name: "Setting", bundle: nil).instantiateViewController(withIdentifier: "RWSettingVC") as! RWSettingVC
        var navigationSetting: UINavigationController! = nil
        navigationSetting = UINavigationController(rootViewController: setting)
        navigationSetting!.tabBarItem = UITabBarItem(title: "Setting", image: UIImage(named: "setting"), tag: 0)
        navigationSetting!.tabBarItem.selectedImage = UIImage(named: "setting")
       // navigationSetting.tabBarItem.selectedImage.
        tabBarItemArray.add(navigationSetting)
        
        
        self.setViewControllers(tabBarItemArray as! [UIViewController], animated: false)
        self.tabBar.tintColor = UIColor.rwColorBlueButton()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    

}
