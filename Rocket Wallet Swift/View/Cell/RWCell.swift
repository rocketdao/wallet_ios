//
//  RWCell.swift
//  Rocket Wallet Swift
//
//  Created by Иван Петрашко on 23.07.2018.
//  Copyright © 2018 Иван§ Петрашко. All rights reserved.
//

import Foundation
import SwipeCellKit
import Charts
import RealmSwift

class RWCellToken:SwipeTableViewCell {
    @IBOutlet weak var logo: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var rate: UILabel!
    @IBOutlet weak var count: UILabel!
    @IBOutlet weak var conversion: UILabel!
    @IBOutlet weak var changerate: UILabel!
    @IBOutlet weak var arr: UIImageView!
    
    func setStyle() -> Void {
         self.name.font = UIFont.setFontStyle(style: .SemiLight, size: 18)
         self.count.font = UIFont.setFontStyle(style: .SemiLight, size: 18)
    }
}

class RWCellTokenList: UITableViewCell {
    @IBOutlet weak var logo: UIImageView!
    @IBOutlet weak var name: UILabel!
}

class RWCellCreateBillSendTo: UITableViewCell {
    @IBOutlet weak var scanButton: UIButton!
    @IBOutlet weak var address: UITextField!
    @IBOutlet weak var sendToLabel: UILabel!
    
     func setStyle() -> Void {
        self.scanButton.setTitleColor(UIColor.rwColorBlueButton(), for: .normal)
        self.scanButton.titleLabel?.font = UIFont.setFontStyle(style: .SemiLight, size: 15)
        
        self.sendToLabel.textColor = UIColor.darkGray
        self.sendToLabel.font = UIFont.setFontStyle(style: .SemiLight, size: 21)
        
        self.address.placeholder = "Address"
        self.address.font = UIFont.setFontStyle(style: .SemiLight, size: 15)
    }
}

class RWCellCreateBillTaxFee: UITableViewCell {
    @IBOutlet weak var slider: UISlider!
    
    @IBOutlet weak var fee: UILabel!
    @IBOutlet weak var feesum: UIButton!
    @IBOutlet weak var feeSpeed: UILabel!
    
    func setStyle() -> Void {
        self.fee.font = UIFont.setFontStyle(style: .SemiLight, size: 14)
        self.feesum.titleLabel?.font = UIFont.setFontStyle(style: .SemiLight, size: 14)
        self.feesum.setTitleColor(UIColor.rwColorBlueButton(), for: .normal)
        self.fee.textColor = UIColor.rwColorGrayGraph()
        self.slider.tintColor = UIColor.rwColorBlueButton()
        self.feeSpeed.font = UIFont.setFontStyle(style: .SemiLight, size: 14)
    }
}

class RWCellCreateBillAmount: UITableViewCell {
    @IBOutlet weak var useall: UIButton!
    @IBOutlet weak var sumLabel: UILabel!
    @IBOutlet weak var fieldSum: UITextField!
    @IBOutlet weak var perevodLabel: UILabel!
    @IBOutlet weak var perevodsum: UILabel!
    @IBOutlet weak var changecurrency: UIButton!
    @IBOutlet weak var labelAmount: UILabel!
    
    func setStyle() -> Void {
        self.perevodLabel.font = UIFont.setFontStyle(style: .SemiLight, size: 15)
        self.perevodsum.font = UIFont.setFontStyle(style: .SemiLight, size: 15)
        self.perevodsum.textColor = UIColor.rwColorGrayGraph()
        self.perevodLabel.textColor = UIColor.rwColorGrayGraph()
        
        
        self.labelAmount.textColor = UIColor.darkGray
        self.labelAmount.font = UIFont.setFontStyle(style: .SemiLight, size: 21)
        
        self.sumLabel.textColor = UIColor.black
        self.sumLabel.font = UIFont.setFontStyle(style: .SemiLight, size: 15)
        
        self.useall.setTitleColor(UIColor.rwColorBlueButton(), for: .normal)
        self.useall.titleLabel?.font = UIFont.setFontStyle(style: .SemiLight, size: 15)
        
        self.fieldSum.placeholder = "Enter amount"
        
        self.changecurrency.setTitleColor(UIColor.rwColorBlueButton(), for: .normal)
        self.changecurrency.titleLabel?.font = UIFont.setFontStyle(style: .SemiLight, size: 15)
        
        self.fieldSum.font = UIFont.setFontStyle(style: .SemiLight, size: 15)
    }
}

class RWCellTokenHeader: UITableViewCell {
    @IBOutlet weak var viewLabel: UIView!
    @IBOutlet weak var imageProfile: UIImageView!
    @IBOutlet weak var eth: UILabel!
    @IBOutlet weak var sum: UILabel!
    @IBOutlet weak var sumchange: UILabel!
    @IBOutlet weak var percentchange: UILabel!
    @IBOutlet weak var currency: UIButton!
    @IBOutlet weak var h24: UILabel!
    @IBOutlet weak var constraitArrow: NSLayoutConstraint!
   // @IBOutlet weak var arrow: UIImageView!
    @IBOutlet weak var arr: UIImageView!
}

extension RWCellTokenGraph: IAxisValueFormatter {
    func stringForValue(_ value: Double, axis: AxisBase?) -> String {
        
       // let entry = self.graph.lineData?.dataSets[Int(value)]
         let entry = self.graph.lineData?.dataSets[0].entryForIndex(Int(value))
        let balance = entry?.data as! RWBalance
        
        return balance.date.getDateString(byFormat: formatByPeriod[periodOnline])!
        //return dataLabels[min(max(Int(value), 0), dataLabels.count - 1)]
    }
}


class RWCellTokenGraph: UITableViewCell,ChartViewDelegate,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    var update = true
    var periodOnline = 0
    let formatByPeriod = ["H", "d", "d",
                      "MMM", "MMM", "MMM",
                      "MMM", "YYYY", "YYYY"]
    
    let formatByPeriodSeleced = ["d MMM. yyyy  HH:mm", "d MMM. yyyy  HH:mm", "d MMM. yyyy",
                          "d MMM. yyyy", "d MMM. yyyy", "d MMM. yyyy",
                          "d MMM. yyyy", "d MMM. yyyy", "d MMM. yyyy"]
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return Constants.GRAPH_ARRAY_PERIOD.count
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if periodOnline != indexPath.row {
            let period = periodOnline
            periodOnline = indexPath.row
            NotificationCenter.default.post(name: NSNotification.Name("setPeriodOnline"), object: NSNumber.init(value: indexPath.row))
            //update = true
            //self.table.reloadData()
            self.indicator.isHidden = false
            self.indicator.startAnimating()
        
            UIView.performWithoutAnimation {
                collectionView.reloadItems(at: [indexPath,IndexPath(item: period, section: 0)])
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = table.dequeueReusableCell(withReuseIdentifier: "CellCollection", for: indexPath) as! RWCellGraphList
        cell.transactionName.setTitle(Constants.GRAPH_ARRAY_PERIOD[indexPath.row], for: .normal)
        cell.transactionName.titleLabel?.font = UIFont.setFontStyle(style: .SemiLight, size: 13)
        cell.transactionName.isUserInteractionEnabled = false
        if periodOnline != indexPath.row {
            cell.transactionName.setTitleColor(UIColor.rwColorGrayGraph(), for: .normal)
        } else {
            cell.transactionName.setTitleColor(UIColor.rwColorBlueButton(), for: .normal)
        }
        return cell;
    }
    
    func graphInit(indicat:Bool){
        self.graph.delegate = self as ChartViewDelegate
        self.graph.chartDescription?.enabled = false
        self.graph.dragEnabled = true
        self.graph.setScaleEnabled(false)
        self.graph.pinchZoomEnabled = false
        self.graph.xAxis.drawAxisLineEnabled = false
        self.graph.xAxis.drawGridLinesEnabled = false
        self.graph.leftAxis.drawAxisLineEnabled = false
        self.graph.leftAxis.drawGridLinesEnabled = false
        self.graph.rightAxis.drawAxisLineEnabled = false
        self.graph.rightAxis.drawGridLinesEnabled = false
        self.graph.xAxis.labelPosition = .bottom
        let limitLine = ChartLimitLine(limit: 0, label: "")
        limitLine.lineColor = UIColor.black.withAlphaComponent(0.3)
        limitLine.lineWidth = 3
        self.graph.rightAxis.addLimitLine(limitLine)
        self.graph.rightAxis.drawLabelsEnabled = false
        
        
        let xAxis = self.graph.xAxis
        //xAxis.labelPosition = .bottom
        //xAxis.labelFont = .systemFont(ofSize: 10)
       // xAxis.granularity = 1
        xAxis.labelCount = 6
        //xAxis.valueFormatter = DayAxisValueFormatter(chart: self.graph)
        
       // let xAxis = self.graph.xAxis
        xAxis.labelPosition = .bottom
        xAxis.axisMinimum = 0
        xAxis.granularity = 1
        xAxis.valueFormatter = self
        xAxis.labelFont = UIFont.setFontStyle(style: .SemiLight, size: 13)
        xAxis.labelTextColor = UIColor(red:0.56, green:0.56, blue:0.58, alpha:1)
        
        let yAxis = self.graph.leftAxis
        yAxis.labelFont = UIFont.setFontStyle(style: .SemiLight, size: 13)
        yAxis.labelTextColor = UIColor.darkGray
        let marker = MarkerImage.init()
        
        marker.image = UIImage(named: "elips")
        marker.offset = CGPoint.init(x: -3, y: -3)
        self.graph.marker = marker
        
        self.graph.legend.form = .line
        self.graph.legend.enabled = false
        
        //if self.graph.data?.dataSets.
        
        self.indicator.isHidden = indicat
        self.indicator.startAnimating()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (UIScreen.main.bounds.width-10)/CGFloat(Constants.GRAPH_ARRAY_PERIOD.count), height: 26)
    }
    
    @IBOutlet weak var viewLabel: UIView!
    
    @IBOutlet weak var viewPeriod: UIView!
    @IBOutlet weak var graph: LineChartView!
    @IBOutlet weak var sum: UILabel!
    @IBOutlet weak var date: UILabel!
    
    @IBOutlet weak var table: UICollectionView!
    
    func chartValueSelected(_ chartView: ChartViewBase, entry: ChartDataEntry, highlight: Highlight) {
        let balance = entry.data as! RWBalance
        setTextHeader(balance: balance)
    }
    
    func setStyleLabels(){
        self.sum.textColor = UIColor.rwColorBlueButton()
        self.date.textColor = UIColor.rwColorGrayGraph()
        
        self.sum.text = ""
        self.date.text = ""
        
        
        self.sum.font = UIFont.setFontStyle(style: .SemiLight, size: 13)
        self.date.font = UIFont.setFontStyle(style: .SemiLight, size: 13)
    }
    
    func setTextHeader(balance:RWBalance){
        self.sum.text = String(format: "USD %.2f",balance.totalUSD)//"USD 376.4"
        self.date.text = balance.date.getDateString(byFormat: formatByPeriodSeleced[periodOnline])
    }
    
    func chartValueNothingSelected(_ chartView: ChartViewBase) {
      //  self.viewPeriod.isHidden = false;
       // self.viewLabel.isHidden = true;
    }
    
}

class RWCellTransaction: UITableViewCell,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var table: UITableView!
    var delegate: RWTokenDetailVC?
    var transactions:List<RWTransaction> = List.init()
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print(transactions)
        return transactions.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 42
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegate?.gotoTransaction(int: indexPath.row)
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellTransactionSingle", for: indexPath) as! RWCellTransactionSingle
        cell.transactionName.text = transactions[indexPath.row].hashString
        cell.sumInCurr.text = String(format: "%@ %@",transactions[indexPath.row].tokenType, transactions[indexPath.row].value!)
        cell.status.text = String(format: "Status %d", transactions[indexPath.row].confirmations)
        cell.time.text = Date().timeAgoSinceDate(transactions[indexPath.row].date, numericDates: true)
        cell.sum.text =  String(format: "USD %.2f",(transactions[indexPath.row].usdval?.doubleValue)!)
        if transactions[indexPath.row].received {
            cell.arrowImage.image = UIImage(named: "Green-arrow")
        } else {
            cell.arrowImage.image = UIImage(named: "Red-arrow")
        }
        cell.setStyle()
        return cell;
    }
}

class RWCellTransactionSingle: UITableViewCell {
    @IBOutlet weak var transactionName: UILabel!
    @IBOutlet weak var sumInCurr: UILabel!
    @IBOutlet weak var status: UILabel!
    @IBOutlet weak var sum: UILabel!
    @IBOutlet weak var time: UILabel!
    @IBOutlet weak var arrowImage: UIImageView!
    
    func setStyle() -> Void {
        self.transactionName.textColor = UIColor.darkGray
        self.status.textColor = UIColor.darkGray
        self.sumInCurr.textColor = UIColor.darkGray
        self.status.textColor = UIColor.darkGray
        self.time.textColor = UIColor.darkGray
        
        self.transactionName.font = UIFont.setFontStyle(style: .SemiLight, size: 15)
        self.sum.font = UIFont.setFontStyle(style: .SemiLight, size: 15)

        self.status.font = UIFont.setFontStyle(style: .SemiLight, size: 11)
        self.sumInCurr.font = UIFont.setFontStyle(style: .SemiLight, size: 11)
        self.time.font = UIFont.setFontStyle(style: .SemiLight, size: 11)
    }
}

class RWCellGraphList: UICollectionViewCell {
    @IBOutlet weak var transactionName: UIButton!
}

class RWCellBillLabel: UITableViewCell {
    @IBOutlet weak var label: UILabel!
}

class RWCellBillAdress: UITableViewCell {
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var address: UILabel!
}

class RWCellBillAmount: UITableViewCell {
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var count: UILabel!
    @IBOutlet weak var currency: UILabel!
}

class RWCellBillSendButton: UITableViewCell {
    @IBOutlet weak var send: UIButton!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
}

class RWCellBillSendPasscode: UITableViewCell {
    @IBOutlet weak var stack: UIStackView!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
}

class RWCellPasscodeDot: UICollectionViewCell {
    
    @IBOutlet weak var dot: UIView!
}

